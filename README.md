# README #

Document Landscape Analyzer (DLA) is a standard desktop appliction to analyse a given data source (Excelsheets), containing information about the currently present documents and workflows within an oranganizational environment.
Conclusivley, the software delivers results about the necessity and integration options of these documents within an Document Managementsystem (DMS). 

### Background ###
This application was realized during a study project at the University of Applied Science Landshut and should therefore only be used and seen within an educational scope.
The repository also has a ticketing system enabled, where any user can report detected bugs and errors. These may or may not be fixed in upcomming versions.

### Installation ###
The complete source code is provided within this repository. Therefore, the application can manually be built via Visual Studio. 

Nontheless, a precompiled version of the appliction is available for download from https://bitbucket.org/aaigner/document-landscape-analyzer/downloads/DLA.exe

### Documentation ###
A comprehensive instruction and tutorial section is included within this repository also. However, this wiki section was conducted in German.

### License ###
This software is provided as is. Therefore, no warranty or any other responsibilites will be provided. The accuracy of the output results can not be guaranteed also.

The software uses the free library iTextSharp for PDF generation (AGPL). More information at https://sourceforge.net/projects/itextsharp/