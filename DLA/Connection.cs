﻿namespace DLA
{
    public class Connection
    {
        public string source, destination;
        public bool bidirectional;

        public Connection(string pSource, string pDestination)
        {
            this.bidirectional = false;
            this.source = pSource;
            this.destination = pDestination;
        }
    }
}
