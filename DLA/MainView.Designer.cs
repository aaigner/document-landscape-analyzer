﻿namespace DLA
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainView));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btShowTop20 = new System.Windows.Forms.Button();
            this.txtWorkflowCount = new System.Windows.Forms.Label();
            this.txtWorkflowSelector = new System.Windows.Forms.ComboBox();
            this.btOpenWorkflowAnalyzer = new System.Windows.Forms.Button();
            this.txtProgress = new System.Windows.Forms.ProgressBar();
            this.btStop = new System.Windows.Forms.Button();
            this.btStartDLA = new System.Windows.Forms.Button();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtInfoBox = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtFilterByResult = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtFilterByWorkflowCriteria = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFilterByTrayCriteria = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtFilterByOutputFormat = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFilterByInputFormat = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtFilterByAccessFormat = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFilterByDepartment = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtListAllDocuments = new System.Windows.Forms.ListView();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.btViewWorkflow = new System.Windows.Forms.ToolStripButton();
            this.btExportAllWorkflows = new System.Windows.Forms.ToolStripButton();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.txtProblemList = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btExportProblemReport = new System.Windows.Forms.ToolStripButton();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.txtConfigColumnIndexNotes = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtConfigColumnIndexNotifiedDepartments = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtConfigColumnIndexTrayCriteria = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtConfigColumnIndexFrequency = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtConfigColumnIndexImprovements = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtConfigColumnIndexDestinationDepartment = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtConfigColumnIndexSourceDepartment = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtConfigColumnIndexOutputFormat = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtConfigColumnIndexInputFormat = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtConfigColumnIndexAccessFormat = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtConfigColumnIndexIsEditor = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtConfigColumnIndexIsModificator = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtConfigColumnIndexIsCreator = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtConfigColumnIndexDocumentDescription = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtConfigColumnIndexName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtLogWindow = new System.Windows.Forms.RichTextBox();
            this.btSaveReport = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.txtFilterDepartment = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.txtLogicalFilter = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.txtAccessFormatFilter = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.txtInputFormatFilter = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabel5 = new System.Windows.Forms.ToolStripLabel();
            this.txtOutputFormatFilter = new System.Windows.Forms.ToolStripComboBox();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Location = new System.Drawing.Point(0, 625);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1384, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer1.Size = new System.Drawing.Size(1384, 625);
            this.splitContainer1.SplitterDistance = 230;
            this.splitContainer1.TabIndex = 1;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Margin = new System.Windows.Forms.Padding(10, 10, 10, 10);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Panel2.Margin = new System.Windows.Forms.Padding(10, 10, 10, 10);
            this.splitContainer2.Size = new System.Drawing.Size(230, 625);
            this.splitContainer2.SplitterDistance = 180;
            this.splitContainer2.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btShowTop20);
            this.groupBox1.Controls.Add(this.txtWorkflowCount);
            this.groupBox1.Controls.Add(this.txtWorkflowSelector);
            this.groupBox1.Controls.Add(this.btOpenWorkflowAnalyzer);
            this.groupBox1.Controls.Add(this.txtProgress);
            this.groupBox1.Controls.Add(this.btStop);
            this.groupBox1.Controls.Add(this.btStartDLA);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(10, 10, 10, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(10, 10, 10, 10);
            this.groupBox1.Size = new System.Drawing.Size(230, 180);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Control";
            // 
            // btShowTop20
            // 
            this.btShowTop20.Dock = System.Windows.Forms.DockStyle.Top;
            this.btShowTop20.Location = new System.Drawing.Point(10, 92);
            this.btShowTop20.Name = "btShowTop20";
            this.btShowTop20.Size = new System.Drawing.Size(210, 23);
            this.btShowTop20.TabIndex = 6;
            this.btShowTop20.Text = "Top20 Anzeigen";
            this.btShowTop20.UseVisualStyleBackColor = true;
            this.btShowTop20.Click += new System.EventHandler(this.btShowTop20_Click);
            // 
            // txtWorkflowCount
            // 
            this.txtWorkflowCount.AutoSize = true;
            this.txtWorkflowCount.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtWorkflowCount.Location = new System.Drawing.Point(10, 113);
            this.txtWorkflowCount.Name = "txtWorkflowCount";
            this.txtWorkflowCount.Size = new System.Drawing.Size(52, 13);
            this.txtWorkflowCount.TabIndex = 5;
            this.txtWorkflowCount.Text = "Workflow";
            // 
            // txtWorkflowSelector
            // 
            this.txtWorkflowSelector.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtWorkflowSelector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtWorkflowSelector.FormattingEnabled = true;
            this.txtWorkflowSelector.Location = new System.Drawing.Point(10, 126);
            this.txtWorkflowSelector.Name = "txtWorkflowSelector";
            this.txtWorkflowSelector.Size = new System.Drawing.Size(210, 21);
            this.txtWorkflowSelector.TabIndex = 4;
            this.txtWorkflowSelector.SelectedIndexChanged += new System.EventHandler(this.txtWorkflowSelector_SelectedIndexChanged);
            // 
            // btOpenWorkflowAnalyzer
            // 
            this.btOpenWorkflowAnalyzer.Dock = System.Windows.Forms.DockStyle.Top;
            this.btOpenWorkflowAnalyzer.Location = new System.Drawing.Point(10, 69);
            this.btOpenWorkflowAnalyzer.Name = "btOpenWorkflowAnalyzer";
            this.btOpenWorkflowAnalyzer.Size = new System.Drawing.Size(210, 23);
            this.btOpenWorkflowAnalyzer.TabIndex = 3;
            this.btOpenWorkflowAnalyzer.Text = "Workflow Analyzer";
            this.btOpenWorkflowAnalyzer.UseVisualStyleBackColor = true;
            this.btOpenWorkflowAnalyzer.Click += new System.EventHandler(this.btOpenWorkflowAnalyzer_Click);
            // 
            // txtProgress
            // 
            this.txtProgress.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtProgress.Location = new System.Drawing.Point(10, 147);
            this.txtProgress.Name = "txtProgress";
            this.txtProgress.Size = new System.Drawing.Size(210, 23);
            this.txtProgress.TabIndex = 2;
            // 
            // btStop
            // 
            this.btStop.Dock = System.Windows.Forms.DockStyle.Top;
            this.btStop.Location = new System.Drawing.Point(10, 46);
            this.btStop.Name = "btStop";
            this.btStop.Size = new System.Drawing.Size(210, 23);
            this.btStop.TabIndex = 1;
            this.btStop.Text = "Stop";
            this.btStop.UseVisualStyleBackColor = true;
            this.btStop.Click += new System.EventHandler(this.btStop_Click);
            // 
            // btStartDLA
            // 
            this.btStartDLA.Dock = System.Windows.Forms.DockStyle.Top;
            this.btStartDLA.Location = new System.Drawing.Point(10, 23);
            this.btStartDLA.Name = "btStartDLA";
            this.btStartDLA.Size = new System.Drawing.Size(210, 23);
            this.btStartDLA.TabIndex = 0;
            this.btStartDLA.Text = "Starte DLA";
            this.btStartDLA.UseVisualStyleBackColor = true;
            this.btStartDLA.Click += new System.EventHandler(this.btStartDLA_Click);
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.groupBox2);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.groupBox4);
            this.splitContainer3.Size = new System.Drawing.Size(230, 441);
            this.splitContainer3.SplitterDistance = 95;
            this.splitContainer3.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtInfoBox);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(230, 95);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Info Box";
            // 
            // txtInfoBox
            // 
            this.txtInfoBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.txtInfoBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtInfoBox.Location = new System.Drawing.Point(3, 16);
            this.txtInfoBox.Name = "txtInfoBox";
            this.txtInfoBox.Size = new System.Drawing.Size(224, 76);
            this.txtInfoBox.TabIndex = 0;
            this.txtInfoBox.UseCompatibleStateImageBehavior = false;
            this.txtInfoBox.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Eigenschaft";
            this.columnHeader1.Width = 97;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Wert";
            this.columnHeader2.Width = 102;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtFilterByResult);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.txtFilterByWorkflowCriteria);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.txtFilterByTrayCriteria);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.txtFilterByOutputFormat);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.txtFilterByInputFormat);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.txtFilterByAccessFormat);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.txtFilterByDepartment);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(0, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(230, 342);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Filter";
            // 
            // txtFilterByResult
            // 
            this.txtFilterByResult.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtFilterByResult.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtFilterByResult.FormattingEnabled = true;
            this.txtFilterByResult.Items.AddRange(new object[] {
            "---",
            "Reine Ablage",
            "Erweiterte Ablage",
            "DMS würdig (momentan nicht umsetzbar)",
            "DMS würdig (ok)"});
            this.txtFilterByResult.Location = new System.Drawing.Point(3, 233);
            this.txtFilterByResult.Name = "txtFilterByResult";
            this.txtFilterByResult.Size = new System.Drawing.Size(224, 21);
            this.txtFilterByResult.TabIndex = 13;
            this.txtFilterByResult.SelectedIndexChanged += new System.EventHandler(this.txtLogicalFilter_SelectedIndexChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Dock = System.Windows.Forms.DockStyle.Top;
            this.label23.Location = new System.Drawing.Point(3, 220);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(95, 13);
            this.label23.TabIndex = 12;
            this.label23.Text = "nach Klassifikation";
            // 
            // txtFilterByWorkflowCriteria
            // 
            this.txtFilterByWorkflowCriteria.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtFilterByWorkflowCriteria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtFilterByWorkflowCriteria.FormattingEnabled = true;
            this.txtFilterByWorkflowCriteria.Location = new System.Drawing.Point(3, 199);
            this.txtFilterByWorkflowCriteria.Name = "txtFilterByWorkflowCriteria";
            this.txtFilterByWorkflowCriteria.Size = new System.Drawing.Size(224, 21);
            this.txtFilterByWorkflowCriteria.TabIndex = 11;
            this.txtFilterByWorkflowCriteria.SelectedIndexChanged += new System.EventHandler(this.txtLogicalFilter_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Location = new System.Drawing.Point(3, 186);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "nach Workflowkriterien";
            // 
            // txtFilterByTrayCriteria
            // 
            this.txtFilterByTrayCriteria.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtFilterByTrayCriteria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtFilterByTrayCriteria.FormattingEnabled = true;
            this.txtFilterByTrayCriteria.Location = new System.Drawing.Point(3, 165);
            this.txtFilterByTrayCriteria.Name = "txtFilterByTrayCriteria";
            this.txtFilterByTrayCriteria.Size = new System.Drawing.Size(224, 21);
            this.txtFilterByTrayCriteria.TabIndex = 9;
            this.txtFilterByTrayCriteria.SelectedIndexChanged += new System.EventHandler(this.txtLogicalFilter_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Location = new System.Drawing.Point(3, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "nach Ablagekriterien";
            // 
            // txtFilterByOutputFormat
            // 
            this.txtFilterByOutputFormat.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtFilterByOutputFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtFilterByOutputFormat.FormattingEnabled = true;
            this.txtFilterByOutputFormat.Location = new System.Drawing.Point(3, 131);
            this.txtFilterByOutputFormat.Name = "txtFilterByOutputFormat";
            this.txtFilterByOutputFormat.Size = new System.Drawing.Size(224, 21);
            this.txtFilterByOutputFormat.TabIndex = 7;
            this.txtFilterByOutputFormat.SelectedIndexChanged += new System.EventHandler(this.txtOutputFormatFilter_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Location = new System.Drawing.Point(3, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "nach Output-Format";
            // 
            // txtFilterByInputFormat
            // 
            this.txtFilterByInputFormat.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtFilterByInputFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtFilterByInputFormat.FormattingEnabled = true;
            this.txtFilterByInputFormat.Location = new System.Drawing.Point(3, 97);
            this.txtFilterByInputFormat.Name = "txtFilterByInputFormat";
            this.txtFilterByInputFormat.Size = new System.Drawing.Size(224, 21);
            this.txtFilterByInputFormat.TabIndex = 5;
            this.txtFilterByInputFormat.SelectedIndexChanged += new System.EventHandler(this.txtInputFormatFilter_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Location = new System.Drawing.Point(3, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "nach Input-Format";
            // 
            // txtFilterByAccessFormat
            // 
            this.txtFilterByAccessFormat.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtFilterByAccessFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtFilterByAccessFormat.FormattingEnabled = true;
            this.txtFilterByAccessFormat.Location = new System.Drawing.Point(3, 63);
            this.txtFilterByAccessFormat.Name = "txtFilterByAccessFormat";
            this.txtFilterByAccessFormat.Size = new System.Drawing.Size(224, 21);
            this.txtFilterByAccessFormat.TabIndex = 3;
            this.txtFilterByAccessFormat.SelectedIndexChanged += new System.EventHandler(this.txtAccessFormatFilter_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Location = new System.Drawing.Point(3, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "nach Format";
            // 
            // txtFilterByDepartment
            // 
            this.txtFilterByDepartment.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtFilterByDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtFilterByDepartment.FormattingEnabled = true;
            this.txtFilterByDepartment.Location = new System.Drawing.Point(3, 29);
            this.txtFilterByDepartment.Name = "txtFilterByDepartment";
            this.txtFilterByDepartment.Size = new System.Drawing.Size(224, 21);
            this.txtFilterByDepartment.TabIndex = 1;
            this.txtFilterByDepartment.SelectedIndexChanged += new System.EventHandler(this.txtFilterDepartment_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Location = new System.Drawing.Point(3, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "nach Abteilung";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tabControl1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1150, 625);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Output";
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 16);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1144, 606);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtListAllDocuments);
            this.tabPage1.Controls.Add(this.toolStrip2);
            this.tabPage1.Location = new System.Drawing.Point(4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(1136, 580);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Dokumente";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtListAllDocuments
            // 
            this.txtListAllDocuments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtListAllDocuments.Location = new System.Drawing.Point(3, 28);
            this.txtListAllDocuments.Name = "txtListAllDocuments";
            this.txtListAllDocuments.Size = new System.Drawing.Size(1130, 549);
            this.txtListAllDocuments.TabIndex = 3;
            this.txtListAllDocuments.UseCompatibleStateImageBehavior = false;
            this.txtListAllDocuments.View = System.Windows.Forms.View.Details;
            // 
            // toolStrip2
            // 
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.btExportAllWorkflows,
            this.btViewWorkflow});
            this.toolStrip2.Location = new System.Drawing.Point(3, 3);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(1130, 25);
            this.toolStrip2.TabIndex = 0;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.BackColor = System.Drawing.Color.Salmon;
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(141, 22);
            this.toolStripButton1.Text = "Export - Aktuelle Ansicht";
            this.toolStripButton1.Click += new System.EventHandler(this.btSaveReport_Click);
            // 
            // btViewWorkflow
            // 
            this.btViewWorkflow.BackColor = System.Drawing.Color.ForestGreen;
            this.btViewWorkflow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btViewWorkflow.Image = ((System.Drawing.Image)(resources.GetObject("btViewWorkflow.Image")));
            this.btViewWorkflow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btViewWorkflow.Name = "btViewWorkflow";
            this.btViewWorkflow.Size = new System.Drawing.Size(105, 22);
            this.btViewWorkflow.Text = "Workflow Ansicht";
            this.btViewWorkflow.Click += new System.EventHandler(this.btViewWorkflow_Click);
            // 
            // btExportAllWorkflows
            // 
            this.btExportAllWorkflows.BackColor = System.Drawing.Color.DarkKhaki;
            this.btExportAllWorkflows.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btExportAllWorkflows.Image = ((System.Drawing.Image)(resources.GetObject("btExportAllWorkflows.Image")));
            this.btExportAllWorkflows.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btExportAllWorkflows.Name = "btExportAllWorkflows";
            this.btExportAllWorkflows.Size = new System.Drawing.Size(134, 22);
            this.btExportAllWorkflows.Text = "Export - Alle Workflows";
            this.btExportAllWorkflows.Click += new System.EventHandler(this.btExportAllWorkflows_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.txtProblemList);
            this.tabPage3.Controls.Add(this.toolStrip1);
            this.tabPage3.Location = new System.Drawing.Point(4, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage3.Size = new System.Drawing.Size(1136, 580);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Problem Report";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // txtProblemList
            // 
            this.txtProblemList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.txtProblemList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtProblemList.Location = new System.Drawing.Point(3, 28);
            this.txtProblemList.Name = "txtProblemList";
            this.txtProblemList.Size = new System.Drawing.Size(1130, 549);
            this.txtProblemList.TabIndex = 1;
            this.txtProblemList.UseCompatibleStateImageBehavior = false;
            this.txtProblemList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "#";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Document";
            this.columnHeader4.Width = 200;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Department";
            this.columnHeader5.Width = 200;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Problem";
            this.columnHeader6.Width = 400;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btExportProblemReport});
            this.toolStrip1.Location = new System.Drawing.Point(3, 3);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1130, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btExportProblemReport
            // 
            this.btExportProblemReport.BackColor = System.Drawing.Color.Salmon;
            this.btExportProblemReport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btExportProblemReport.Image = ((System.Drawing.Image)(resources.GetObject("btExportProblemReport.Image")));
            this.btExportProblemReport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btExportProblemReport.Name = "btExportProblemReport";
            this.btExportProblemReport.Size = new System.Drawing.Size(130, 22);
            this.btExportProblemReport.Text = "Export Problem Report";
            this.btExportProblemReport.Click += new System.EventHandler(this.btExportProblemReport_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.Silver;
            this.tabPage4.Controls.Add(this.txtConfigColumnIndexNotes);
            this.tabPage4.Controls.Add(this.label22);
            this.tabPage4.Controls.Add(this.txtConfigColumnIndexNotifiedDepartments);
            this.tabPage4.Controls.Add(this.label21);
            this.tabPage4.Controls.Add(this.txtConfigColumnIndexTrayCriteria);
            this.tabPage4.Controls.Add(this.label20);
            this.tabPage4.Controls.Add(this.txtConfigColumnIndexFrequency);
            this.tabPage4.Controls.Add(this.label19);
            this.tabPage4.Controls.Add(this.txtConfigColumnIndexImprovements);
            this.tabPage4.Controls.Add(this.label18);
            this.tabPage4.Controls.Add(this.txtConfigColumnIndexDestinationDepartment);
            this.tabPage4.Controls.Add(this.label17);
            this.tabPage4.Controls.Add(this.txtConfigColumnIndexSourceDepartment);
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Controls.Add(this.txtConfigColumnIndexOutputFormat);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.txtConfigColumnIndexInputFormat);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.txtConfigColumnIndexAccessFormat);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Controls.Add(this.txtConfigColumnIndexIsEditor);
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Controls.Add(this.txtConfigColumnIndexIsModificator);
            this.tabPage4.Controls.Add(this.label11);
            this.tabPage4.Controls.Add(this.txtConfigColumnIndexIsCreator);
            this.tabPage4.Controls.Add(this.label10);
            this.tabPage4.Controls.Add(this.txtConfigColumnIndexDocumentDescription);
            this.tabPage4.Controls.Add(this.label9);
            this.tabPage4.Controls.Add(this.txtConfigColumnIndexName);
            this.tabPage4.Controls.Add(this.label8);
            this.tabPage4.Location = new System.Drawing.Point(4, 4);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(10, 10, 10, 10);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(10, 10, 10, 10);
            this.tabPage4.Size = new System.Drawing.Size(1136, 580);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Konfiguration";
            // 
            // txtConfigColumnIndexNotes
            // 
            this.txtConfigColumnIndexNotes.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtConfigColumnIndexNotes.Location = new System.Drawing.Point(10, 485);
            this.txtConfigColumnIndexNotes.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.txtConfigColumnIndexNotes.Name = "txtConfigColumnIndexNotes";
            this.txtConfigColumnIndexNotes.Size = new System.Drawing.Size(1116, 20);
            this.txtConfigColumnIndexNotes.TabIndex = 29;
            this.txtConfigColumnIndexNotes.Text = "20";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Dock = System.Windows.Forms.DockStyle.Top;
            this.label22.Location = new System.Drawing.Point(10, 472);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(105, 13);
            this.label22.TabIndex = 28;
            this.label22.Text = "Column Index: Notes";
            // 
            // txtConfigColumnIndexNotifiedDepartments
            // 
            this.txtConfigColumnIndexNotifiedDepartments.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtConfigColumnIndexNotifiedDepartments.Location = new System.Drawing.Point(10, 452);
            this.txtConfigColumnIndexNotifiedDepartments.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.txtConfigColumnIndexNotifiedDepartments.Name = "txtConfigColumnIndexNotifiedDepartments";
            this.txtConfigColumnIndexNotifiedDepartments.Size = new System.Drawing.Size(1116, 20);
            this.txtConfigColumnIndexNotifiedDepartments.TabIndex = 27;
            this.txtConfigColumnIndexNotifiedDepartments.Text = "19";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Top;
            this.label21.Location = new System.Drawing.Point(10, 439);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(176, 13);
            this.label21.TabIndex = 26;
            this.label21.Text = "Column Index: Notified Departments";
            // 
            // txtConfigColumnIndexTrayCriteria
            // 
            this.txtConfigColumnIndexTrayCriteria.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtConfigColumnIndexTrayCriteria.Location = new System.Drawing.Point(10, 419);
            this.txtConfigColumnIndexTrayCriteria.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.txtConfigColumnIndexTrayCriteria.Name = "txtConfigColumnIndexTrayCriteria";
            this.txtConfigColumnIndexTrayCriteria.Size = new System.Drawing.Size(1116, 20);
            this.txtConfigColumnIndexTrayCriteria.TabIndex = 25;
            this.txtConfigColumnIndexTrayCriteria.Text = "6;8;9;10;11;17";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Top;
            this.label20.Location = new System.Drawing.Point(10, 406);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(258, 13);
            this.label20.TabIndex = 24;
            this.label20.Text = "Column Index: Tray Criterias (seperated by semicolon)";
            // 
            // txtConfigColumnIndexFrequency
            // 
            this.txtConfigColumnIndexFrequency.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtConfigColumnIndexFrequency.Location = new System.Drawing.Point(10, 386);
            this.txtConfigColumnIndexFrequency.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.txtConfigColumnIndexFrequency.Name = "txtConfigColumnIndexFrequency";
            this.txtConfigColumnIndexFrequency.Size = new System.Drawing.Size(1116, 20);
            this.txtConfigColumnIndexFrequency.TabIndex = 23;
            this.txtConfigColumnIndexFrequency.Text = "18";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Top;
            this.label19.Location = new System.Drawing.Point(10, 373);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(127, 13);
            this.label19.TabIndex = 22;
            this.label19.Text = "Column Index: Frequency";
            // 
            // txtConfigColumnIndexImprovements
            // 
            this.txtConfigColumnIndexImprovements.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtConfigColumnIndexImprovements.Location = new System.Drawing.Point(10, 353);
            this.txtConfigColumnIndexImprovements.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.txtConfigColumnIndexImprovements.Name = "txtConfigColumnIndexImprovements";
            this.txtConfigColumnIndexImprovements.Size = new System.Drawing.Size(1116, 20);
            this.txtConfigColumnIndexImprovements.TabIndex = 21;
            this.txtConfigColumnIndexImprovements.Text = "21";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Top;
            this.label18.Location = new System.Drawing.Point(10, 340);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(143, 13);
            this.label18.TabIndex = 20;
            this.label18.Text = "Column Index: Improvements";
            // 
            // txtConfigColumnIndexDestinationDepartment
            // 
            this.txtConfigColumnIndexDestinationDepartment.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtConfigColumnIndexDestinationDepartment.Location = new System.Drawing.Point(10, 320);
            this.txtConfigColumnIndexDestinationDepartment.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.txtConfigColumnIndexDestinationDepartment.Name = "txtConfigColumnIndexDestinationDepartment";
            this.txtConfigColumnIndexDestinationDepartment.Size = new System.Drawing.Size(1116, 20);
            this.txtConfigColumnIndexDestinationDepartment.TabIndex = 19;
            this.txtConfigColumnIndexDestinationDepartment.Text = "15";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Top;
            this.label17.Location = new System.Drawing.Point(10, 307);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(188, 13);
            this.label17.TabIndex = 18;
            this.label17.Text = "Column Index: Destination Department";
            // 
            // txtConfigColumnIndexSourceDepartment
            // 
            this.txtConfigColumnIndexSourceDepartment.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtConfigColumnIndexSourceDepartment.Location = new System.Drawing.Point(10, 287);
            this.txtConfigColumnIndexSourceDepartment.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.txtConfigColumnIndexSourceDepartment.Name = "txtConfigColumnIndexSourceDepartment";
            this.txtConfigColumnIndexSourceDepartment.Size = new System.Drawing.Size(1116, 20);
            this.txtConfigColumnIndexSourceDepartment.TabIndex = 17;
            this.txtConfigColumnIndexSourceDepartment.Text = "12";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Top;
            this.label16.Location = new System.Drawing.Point(10, 274);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(169, 13);
            this.label16.TabIndex = 16;
            this.label16.Text = "Column Index: Source Department";
            // 
            // txtConfigColumnIndexOutputFormat
            // 
            this.txtConfigColumnIndexOutputFormat.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtConfigColumnIndexOutputFormat.Location = new System.Drawing.Point(10, 254);
            this.txtConfigColumnIndexOutputFormat.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.txtConfigColumnIndexOutputFormat.Name = "txtConfigColumnIndexOutputFormat";
            this.txtConfigColumnIndexOutputFormat.Size = new System.Drawing.Size(1116, 20);
            this.txtConfigColumnIndexOutputFormat.TabIndex = 15;
            this.txtConfigColumnIndexOutputFormat.Text = "16";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Top;
            this.label15.Location = new System.Drawing.Point(10, 241);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(144, 13);
            this.label15.TabIndex = 14;
            this.label15.Text = "Column Index: Output Format";
            // 
            // txtConfigColumnIndexInputFormat
            // 
            this.txtConfigColumnIndexInputFormat.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtConfigColumnIndexInputFormat.Location = new System.Drawing.Point(10, 221);
            this.txtConfigColumnIndexInputFormat.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.txtConfigColumnIndexInputFormat.Name = "txtConfigColumnIndexInputFormat";
            this.txtConfigColumnIndexInputFormat.Size = new System.Drawing.Size(1116, 20);
            this.txtConfigColumnIndexInputFormat.TabIndex = 13;
            this.txtConfigColumnIndexInputFormat.Text = "13";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Top;
            this.label14.Location = new System.Drawing.Point(10, 208);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(136, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "Column Index: Input Format";
            // 
            // txtConfigColumnIndexAccessFormat
            // 
            this.txtConfigColumnIndexAccessFormat.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtConfigColumnIndexAccessFormat.Location = new System.Drawing.Point(10, 188);
            this.txtConfigColumnIndexAccessFormat.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.txtConfigColumnIndexAccessFormat.Name = "txtConfigColumnIndexAccessFormat";
            this.txtConfigColumnIndexAccessFormat.Size = new System.Drawing.Size(1116, 20);
            this.txtConfigColumnIndexAccessFormat.TabIndex = 11;
            this.txtConfigColumnIndexAccessFormat.Text = "7";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Top;
            this.label13.Location = new System.Drawing.Point(10, 175);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(147, 13);
            this.label13.TabIndex = 10;
            this.label13.Text = "Column Index: Access Format";
            // 
            // txtConfigColumnIndexIsEditor
            // 
            this.txtConfigColumnIndexIsEditor.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtConfigColumnIndexIsEditor.Location = new System.Drawing.Point(10, 155);
            this.txtConfigColumnIndexIsEditor.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.txtConfigColumnIndexIsEditor.Name = "txtConfigColumnIndexIsEditor";
            this.txtConfigColumnIndexIsEditor.Size = new System.Drawing.Size(1116, 20);
            this.txtConfigColumnIndexIsEditor.TabIndex = 9;
            this.txtConfigColumnIndexIsEditor.Text = "5";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Top;
            this.label12.Location = new System.Drawing.Point(10, 142);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(115, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Column Index: Is Editor";
            // 
            // txtConfigColumnIndexIsModificator
            // 
            this.txtConfigColumnIndexIsModificator.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtConfigColumnIndexIsModificator.Location = new System.Drawing.Point(10, 122);
            this.txtConfigColumnIndexIsModificator.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.txtConfigColumnIndexIsModificator.Name = "txtConfigColumnIndexIsModificator";
            this.txtConfigColumnIndexIsModificator.Size = new System.Drawing.Size(1116, 20);
            this.txtConfigColumnIndexIsModificator.TabIndex = 7;
            this.txtConfigColumnIndexIsModificator.Text = "4";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Top;
            this.label11.Location = new System.Drawing.Point(10, 109);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(140, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "Column Index: Is Modificator";
            // 
            // txtConfigColumnIndexIsCreator
            // 
            this.txtConfigColumnIndexIsCreator.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtConfigColumnIndexIsCreator.Location = new System.Drawing.Point(10, 89);
            this.txtConfigColumnIndexIsCreator.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.txtConfigColumnIndexIsCreator.Name = "txtConfigColumnIndexIsCreator";
            this.txtConfigColumnIndexIsCreator.Size = new System.Drawing.Size(1116, 20);
            this.txtConfigColumnIndexIsCreator.TabIndex = 5;
            this.txtConfigColumnIndexIsCreator.Text = "3";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Location = new System.Drawing.Point(10, 76);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(122, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Column Index: Is Creator";
            // 
            // txtConfigColumnIndexDocumentDescription
            // 
            this.txtConfigColumnIndexDocumentDescription.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtConfigColumnIndexDocumentDescription.Location = new System.Drawing.Point(10, 56);
            this.txtConfigColumnIndexDocumentDescription.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.txtConfigColumnIndexDocumentDescription.Name = "txtConfigColumnIndexDocumentDescription";
            this.txtConfigColumnIndexDocumentDescription.Size = new System.Drawing.Size(1116, 20);
            this.txtConfigColumnIndexDocumentDescription.TabIndex = 3;
            this.txtConfigColumnIndexDocumentDescription.Text = "2";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Location = new System.Drawing.Point(10, 43);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(182, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Column Index: Document Description";
            // 
            // txtConfigColumnIndexName
            // 
            this.txtConfigColumnIndexName.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtConfigColumnIndexName.Location = new System.Drawing.Point(10, 23);
            this.txtConfigColumnIndexName.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.txtConfigColumnIndexName.Name = "txtConfigColumnIndexName";
            this.txtConfigColumnIndexName.Size = new System.Drawing.Size(1116, 20);
            this.txtConfigColumnIndexName.TabIndex = 1;
            this.txtConfigColumnIndexName.Text = "1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Location = new System.Drawing.Point(10, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(169, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Column Index: of Document Name";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtLogWindow);
            this.tabPage2.Location = new System.Drawing.Point(4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage2.Size = new System.Drawing.Size(1136, 580);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Debug";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtLogWindow
            // 
            this.txtLogWindow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLogWindow.Location = new System.Drawing.Point(3, 3);
            this.txtLogWindow.Name = "txtLogWindow";
            this.txtLogWindow.Size = new System.Drawing.Size(1130, 574);
            this.txtLogWindow.TabIndex = 0;
            this.txtLogWindow.Text = "";
            // 
            // btSaveReport
            // 
            this.btSaveReport.BackColor = System.Drawing.Color.DarkRed;
            this.btSaveReport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btSaveReport.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSaveReport.ForeColor = System.Drawing.Color.White;
            this.btSaveReport.Image = ((System.Drawing.Image)(resources.GetObject("btSaveReport.Image")));
            this.btSaveReport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btSaveReport.Name = "btSaveReport";
            this.btSaveReport.Size = new System.Drawing.Size(90, 22);
            this.btSaveReport.Text = "Export Report";
            this.btSaveReport.Click += new System.EventHandler(this.btSaveReport_Click);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(115, 22);
            this.toolStripLabel2.Text = "Filter by Department";
            // 
            // txtFilterDepartment
            // 
            this.txtFilterDepartment.Name = "txtFilterDepartment";
            this.txtFilterDepartment.Size = new System.Drawing.Size(82, 23);
            this.txtFilterDepartment.SelectedIndexChanged += new System.EventHandler(this.txtFilterDepartment_SelectedIndexChanged);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(74, 22);
            this.toolStripLabel1.Text = "Logical Filter";
            // 
            // txtLogicalFilter
            // 
            this.txtLogicalFilter.Name = "txtLogicalFilter";
            this.txtLogicalFilter.Size = new System.Drawing.Size(82, 23);
            this.txtLogicalFilter.SelectedIndexChanged += new System.EventHandler(this.txtLogicalFilter_SelectedIndexChanged);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(84, 22);
            this.toolStripLabel3.Text = "Access Format";
            // 
            // txtAccessFormatFilter
            // 
            this.txtAccessFormatFilter.Name = "txtAccessFormatFilter";
            this.txtAccessFormatFilter.Size = new System.Drawing.Size(82, 23);
            this.txtAccessFormatFilter.SelectedIndexChanged += new System.EventHandler(this.txtAccessFormatFilter_SelectedIndexChanged);
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(76, 22);
            this.toolStripLabel4.Text = "Input Format";
            // 
            // txtInputFormatFilter
            // 
            this.txtInputFormatFilter.Name = "txtInputFormatFilter";
            this.txtInputFormatFilter.Size = new System.Drawing.Size(82, 23);
            this.txtInputFormatFilter.SelectedIndexChanged += new System.EventHandler(this.txtInputFormatFilter_SelectedIndexChanged);
            // 
            // toolStripLabel5
            // 
            this.toolStripLabel5.Name = "toolStripLabel5";
            this.toolStripLabel5.Size = new System.Drawing.Size(86, 22);
            this.toolStripLabel5.Text = "Output Format";
            // 
            // txtOutputFormatFilter
            // 
            this.txtOutputFormatFilter.Name = "txtOutputFormatFilter";
            this.txtOutputFormatFilter.Size = new System.Drawing.Size(82, 23);
            this.txtOutputFormatFilter.SelectedIndexChanged += new System.EventHandler(this.txtOutputFormatFilter_SelectedIndexChanged);
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 647);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainView";
            this.Text = "Document Landscape Analyzer";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btStartDLA;
        private System.Windows.Forms.Button btStop;
        private System.Windows.Forms.ProgressBar txtProgress;
        private System.Windows.Forms.Button btOpenWorkflowAnalyzer;
        private System.Windows.Forms.Label txtWorkflowCount;
        private System.Windows.Forms.ComboBox txtWorkflowSelector;
        private System.Windows.Forms.ToolStripButton btSaveReport;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripComboBox txtFilterDepartment;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox txtLogicalFilter;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripComboBox txtAccessFormatFilter;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripComboBox txtInputFormatFilter;
        private System.Windows.Forms.ToolStripLabel toolStripLabel5;
        private System.Windows.Forms.ToolStripComboBox txtOutputFormatFilter;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.RichTextBox txtLogWindow;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btExportProblemReport;
        private System.Windows.Forms.ListView txtProblemList;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ListView txtListAllDocuments;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView txtInfoBox;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox txtFilterByDepartment;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox txtFilterByAccessFormat;
        private System.Windows.Forms.ComboBox txtFilterByInputFormat;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox txtFilterByOutputFormat;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox txtFilterByWorkflowCriteria;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox txtFilterByTrayCriteria;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox txtConfigColumnIndexName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtConfigColumnIndexDocumentDescription;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtConfigColumnIndexIsCreator;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtConfigColumnIndexIsEditor;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtConfigColumnIndexIsModificator;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtConfigColumnIndexOutputFormat;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtConfigColumnIndexInputFormat;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtConfigColumnIndexAccessFormat;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtConfigColumnIndexDestinationDepartment;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtConfigColumnIndexSourceDepartment;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtConfigColumnIndexImprovements;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtConfigColumnIndexTrayCriteria;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtConfigColumnIndexFrequency;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtConfigColumnIndexNotifiedDepartments;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtConfigColumnIndexNotes;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox txtFilterByResult;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ToolStripButton btViewWorkflow;
        private System.Windows.Forms.Button btShowTop20;
        private System.Windows.Forms.ToolStripButton btExportAllWorkflows;
    }
}

