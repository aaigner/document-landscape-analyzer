﻿namespace DLA
{
    partial class WorkflowFlowView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WorkflowFlowView));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btExportGraphic = new System.Windows.Forms.ToolStripButton();
            this.btExportReport = new System.Windows.Forms.ToolStripButton();
            this.btExportFullReport = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel = new System.Windows.Forms.Panel();
            this.txtAlertList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStrip1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Location = new System.Drawing.Point(0, 365);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 9, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1134, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btExportGraphic,
            this.btExportReport,
            this.btExportFullReport});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1134, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btExportGraphic
            // 
            this.btExportGraphic.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btExportGraphic.Image = ((System.Drawing.Image)(resources.GetObject("btExportGraphic.Image")));
            this.btExportGraphic.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btExportGraphic.Name = "btExportGraphic";
            this.btExportGraphic.Size = new System.Drawing.Size(86, 22);
            this.btExportGraphic.Text = "Export - Grafik";
            this.btExportGraphic.Click += new System.EventHandler(this.btExportGraphic_Click);
            // 
            // btExportReport
            // 
            this.btExportReport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btExportReport.Image = ((System.Drawing.Image)(resources.GetObject("btExportReport.Image")));
            this.btExportReport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btExportReport.Name = "btExportReport";
            this.btExportReport.Size = new System.Drawing.Size(90, 22);
            this.btExportReport.Text = "Export - Report";
            this.btExportReport.Click += new System.EventHandler(this.btExportReport_Click);
            // 
            // btExportFullReport
            // 
            this.btExportFullReport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btExportFullReport.Image = ((System.Drawing.Image)(resources.GetObject("btExportFullReport.Image")));
            this.btExportFullReport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btExportFullReport.Name = "btExportFullReport";
            this.btExportFullReport.Size = new System.Drawing.Size(153, 22);
            this.btExportFullReport.Text = "Export - Report (mit Grafik)";
            this.btExportFullReport.Click += new System.EventHandler(this.btExportFullReport_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.txtAlertList);
            this.splitContainer1.Size = new System.Drawing.Size(1134, 340);
            this.splitContainer1.SplitterDistance = 218;
            this.splitContainer1.SplitterWidth = 3;
            this.splitContainer1.TabIndex = 2;
            // 
            // panel
            // 
            this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(1134, 218);
            this.panel.TabIndex = 0;
            // 
            // txtAlertList
            // 
            this.txtAlertList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.txtAlertList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAlertList.Location = new System.Drawing.Point(0, 0);
            this.txtAlertList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtAlertList.Name = "txtAlertList";
            this.txtAlertList.Size = new System.Drawing.Size(1134, 119);
            this.txtAlertList.TabIndex = 0;
            this.txtAlertList.UseCompatibleStateImageBehavior = false;
            this.txtAlertList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "#";
            this.columnHeader1.Width = 40;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Abteilung";
            this.columnHeader2.Width = 100;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Kategorie";
            this.columnHeader3.Width = 100;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Problem";
            this.columnHeader4.Width = 200;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "DMS Bewertung";
            this.columnHeader5.Width = 100;
            // 
            // WorkflowFlowView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1134, 387);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "WorkflowFlowView";
            this.ShowIcon = false;
            this.Text = "Workflow Viewer";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btExportGraphic;
        private System.Windows.Forms.ToolStripButton btExportReport;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListView txtAlertList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.ToolStripButton btExportFullReport;
    }
}