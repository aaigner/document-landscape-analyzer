﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DLA
{
    public partial class WorkflowAnalyzerView : Form
    {
        private MainView backLink;
        private Dictionary<string, Document> documents;

        public WorkflowAnalyzerView(ref MainView pBackLink, List<Document> pData)
        {
            InitializeComponent();

            this.backLink = pBackLink;
            this.documents = new Dictionary<string, Document>();
            List<string> txt = new List<string>();

            foreach (Document d in pData)
            {
                string id = d.getName() + " [" + d.getDepartment() + "]";
                this.documents.Add(id, d);
                txt.Add(id);
            }

            txt.Sort();
            this.txtAllDocuments.Items.AddRange(txt.ToArray());
            this.Show();
        }

        private void btAnalyze_Click(object sender, EventArgs e)
        {
            if(this.txtWorkflowSequence.Items.Count > 0)
            {
                List<Document> sequence = new List<Document>();

                foreach(ListViewItem input in this.txtWorkflowSequence.Items)
                {
                    string id = input.SubItems[1].Text;
                    sequence.Add(this.documents[id]);
                }

                string name = (!string.IsNullOrEmpty(this.txtWorkflowName.Text)) ? this.txtWorkflowName.Text : "workflow";
                this.backLink.fillWorkflowSequence(sequence, name);
                this.Close();
            }
        }

        private void btRemoveLast_Click(object sender, EventArgs e)
        {
            if(this.txtWorkflowSequence.Items.Count > 1)
            {
                this.txtWorkflowSequence.Items.RemoveAt(this.txtWorkflowSequence.Items.Count - 1);
            }
        }

        private void btAddDocument_Click(object sender, EventArgs e)
        {
            ListViewItem item = new ListViewItem((this.txtWorkflowSequence.Items.Count + 1).ToString());
            item.SubItems.Add(this.txtAllDocuments.SelectedItem.ToString());
            this.txtWorkflowSequence.Items.Add(item);
        }
    }
}
