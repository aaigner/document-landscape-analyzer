﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace DLA
{
    public partial class MainView : Form
    {
        private const int ROW_START_INDEX = 2;

        private BackgroundWorker worker;
        private string dataDirectory;
        private bool oneSheet;
        private string currentWorkflowName;
        private List<Document> documents;
        private List<Document> sequence;
        private List<Document> top20;
        private List<string> applicableIndicatorsYes;
        private List<string> applicableIndicatorsNo;
        private Dictionary<string, List<Document>> workflows;
        private List<int> trayCriteriaColumns;
        
        public MainView()
        {
            InitializeComponent();

            this.worker = new BackgroundWorker();
            this.worker.WorkerReportsProgress = true;
            this.worker.WorkerSupportsCancellation = true;
            this.worker.ProgressChanged += Worker_ProgressChanged;
            this.worker.DoWork += Worker_DoWork;
            this.worker.RunWorkerCompleted += Worker_RunWorkerCompleted;

            this.clearUI();

            this.btStartDLA.Enabled = true;
            this.btStop.Enabled = false;

            this.documents = new List<Document>();
            this.sequence = new List<Document>();
            this.top20 = new List<Document>();
            this.workflows = new Dictionary<string, List<Document>>();
            this.trayCriteriaColumns = new List<int>();
            
            this.applicableIndicatorsYes = new List<string>();
            this.applicableIndicatorsYes.Add("ja");
            this.applicableIndicatorsYes.Add("ja, automatisch");
            this.applicableIndicatorsYes.Add("ja, manuell");
            this.applicableIndicatorsYes.Add("vertraulich");
            // ...

            this.applicableIndicatorsNo = new List<string>();
            this.applicableIndicatorsNo.Add("nein");
            this.applicableIndicatorsNo.Add("");
            this.applicableIndicatorsNo.Add("nicht vertraulich");
            // ...
            
            this.txtListAllDocuments.Columns.Add("Dokument");
            this.txtListAllDocuments.Columns.Add("Beschreibung");
            this.txtListAllDocuments.Columns.Add("Ersteller");
            this.txtListAllDocuments.Columns.Add("Modifizierer");
            this.txtListAllDocuments.Columns.Add("Bearbeiter");
            this.txtListAllDocuments.Columns.Add("Abteilung");
            this.txtListAllDocuments.Columns.Add("Abteilung - Quelle");
            this.txtListAllDocuments.Columns.Add("Abteilung - Ziel");
            this.txtListAllDocuments.Columns.Add("Format");
            this.txtListAllDocuments.Columns.Add("Format - Input");
            this.txtListAllDocuments.Columns.Add("Format - Output");
            this.txtListAllDocuments.Columns.Add("Ablagekriterien");
            this.txtListAllDocuments.Columns.Add("Verwendungshäufigkeit");
            this.txtListAllDocuments.Columns.Add("benachrichtigte Abteilungen");
            this.txtListAllDocuments.Columns.Add("Ergebnis");
            this.txtListAllDocuments.Columns.Add("Integriert");

            this.txtFilterByAccessFormat.Items.Add("---");
            this.txtFilterByInputFormat.Items.Add("---");
            this.txtFilterByOutputFormat.Items.Add("---");
            this.txtFilterByTrayCriteria.Items.Add("---");
            this.txtFilterByTrayCriteria.Items.Add("erfüllt");
            this.txtFilterByTrayCriteria.Items.Add("nicht erfüllt");
            this.txtFilterByWorkflowCriteria.Items.Add("---");
            this.txtFilterByWorkflowCriteria.Items.Add("erfüllt");
            this.txtFilterByWorkflowCriteria.Items.Add("nicht erfüllt");

            foreach (DocumentFormat da in Enum.GetValues(typeof(DocumentFormat)))
            {
                this.txtFilterByAccessFormat.Items.Add(da.ToString());
            }

            foreach (DocumentFormat da in Enum.GetValues(typeof(DocumentFormat)))
            {
                this.txtFilterByInputFormat.Items.Add(da.ToString());
            }

            foreach (DocumentFormat da in Enum.GetValues(typeof(DocumentFormat)))
            {
                this.txtFilterByOutputFormat.Items.Add(da.ToString());
            }
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.btStartDLA.Enabled = true;
            this.btStop.Enabled = false;

            ListViewItem item1 = new ListViewItem("Anzahl Dokumente");
            item1.SubItems.Add(this.documents.Count.ToString());
            this.txtInfoBox.Items.Add(item1);

            this.txtFilterByDepartment.Items.Clear();
            List<string> departments = new List<string>();
            this.txtFilterByDepartment.Items.Add("---");

            foreach (Document d in this.documents)
            {
                if(!departments.Contains(d.getDepartment()))
                {
                    departments.Add(d.getDepartment());
                    this.txtFilterByDepartment.Items.Add(d.getDepartment());
                }
            }

            this.txtWorkflowCount.Text = "Workflow (" + this.workflows.Count + ")";
            this.fillIdentifiedWorkflowsSelector();
            this.fillAllDocumentsView();

            MessageBox.Show("Task complete", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            if(!string.IsNullOrEmpty(this.dataDirectory))
            {
                try
                {
                    this.sequence = new List<Document>();
                    this.top20 = new List<Document>();
                    this.documents = new List<Document>();
                    this.trayCriteriaColumns.Clear();

                    foreach(string s in this.txtConfigColumnIndexTrayCriteria.Text.Split(';'))
                    {
                        this.trayCriteriaColumns.Add(Convert.ToInt32(s));
                    }

                    if (this.oneSheet)
                    {
                        this.worker.ReportProgress(0, "file: " + this.dataDirectory);

                        this.parseSheet(this.dataDirectory, "Überarbeitet");
                        this.worker.ReportProgress(0);
                    }
                    else
                    {
                        DirectoryInfo di = new DirectoryInfo(this.dataDirectory);
                        FileInfo[] files = di.GetFiles("*.xls*", SearchOption.TopDirectoryOnly);

                        foreach (FileInfo fi in files)
                        {
                            this.worker.ReportProgress(0, "file: " + fi.Name);

                            this.parseSheet(fi.FullName, fi.Name.Substring(0, fi.Name.IndexOf('.')));
                            this.worker.ReportProgress(0);
                        }
                    }

                    this.identifyWorkflows();
                    this.identifyProblems();
                }
                catch (Exception ex)
                {
                    this.worker.ReportProgress(0, ex.Message);
                }
            }
            else
            {
                this.logEvent("Kein gültiges Verzeichnis gewählt");
            }
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState != null)
            {
                this.logEvent(e.UserState.ToString());
            }
            else
            {
                this.txtProgress.PerformStep();
            }
        }

        private void clearUI()
        {
            this.txtLogWindow.Clear();
            this.txtProgress.Value = 0;
            this.txtProgress.Maximum = 100;
            this.txtProgress.Step = 1;
            this.txtInfoBox.Items.Clear();
            this.txtListAllDocuments.GridLines = true;
        }

        private void logEvent(string pMessage)
        {
            string timestamp = "[" + DateTime.Now.ToString("yyyy-MM-dd_H-mm-ss-fff") + "] ";
            this.txtLogWindow.AppendText(timestamp + pMessage + "\n");
        }

        public void parseSheet(string pFilename, string pDepartment)
        {
            try
            {
                DataTable sheet = new DataTable("Excel Sheet");
                OleDbConnectionStringBuilder csbuilder = new OleDbConnectionStringBuilder();
                csbuilder.Provider = "Microsoft.ACE.OLEDB.12.0";
                csbuilder.DataSource = pFilename;
                csbuilder.Add("Extended Properties", "Excel 12.0 Xml;HDR=YES");
                string selectSql = @"SELECT * FROM ["+pDepartment+"$]";
                using (OleDbConnection connection = new OleDbConnection(csbuilder.ConnectionString))
                using (OleDbDataAdapter adapter = new OleDbDataAdapter(selectSql, connection))
                {
                    connection.Open();
                    adapter.Fill(sheet);

                    int counter = 0;

                    if (this.oneSheet)
                    {
                        string currentDepartment = "";

                        for (int i = 0; i < sheet.Rows.Count; i++)
                        {       
                            if (i < ROW_START_INDEX)
                            {
                                continue;
                            }

                            if (string.IsNullOrEmpty(sheet.Rows[i][0].ToString()))
                            {
                                break;
                            }

                            if (sheet.Rows[i][0].ToString().Equals("0"))
                            {
                                currentDepartment = sheet.Rows[i][1].ToString();
                                continue;
                            }

                            this.parseRow(currentDepartment.ToLower(), sheet.Rows[i]);
                            counter++;
                        }

                        this.worker.ReportProgress(0, counter + " zeilen verarbeitet");
                    }
                    else
                    {
                        for (int i = 0; i < sheet.Rows.Count; i++)
                        {
                            if (i < ROW_START_INDEX)
                            {
                                continue;
                            }

                            this.parseRow(pDepartment.ToLower(), sheet.Rows[i]);
                            counter++;
                        }

                        this.worker.ReportProgress(0, counter + " zeilen verarbeitet");
                    }
                }
            }
            catch (Exception ex)
            {
                this.worker.ReportProgress(0, ex.Message);
            }
        }

        public void parseRow(string pDepartment, DataRow pRow)
        {
            try
            {
                string name = pRow[Convert.ToInt32(this.txtConfigColumnIndexName.Text)].ToString().ToLower();

                if(string.IsNullOrEmpty(name))
                {
                    return;
                }

                string description = pRow[Convert.ToInt32(this.txtConfigColumnIndexDocumentDescription.Text)].ToString().ToLower();
                string notes = pRow[Convert.ToInt32(this.txtConfigColumnIndexDestinationDepartment.Text)].ToString().ToLower();
                string improvements = pRow[Convert.ToInt32(this.txtConfigColumnIndexImprovements.Text)].ToString().ToLower();
                string departmentSource = pRow[Convert.ToInt32(this.txtConfigColumnIndexSourceDepartment.Text)].ToString().ToLower();
                string departmentDestination = pRow[Convert.ToInt32(this.txtConfigColumnIndexDestinationDepartment.Text)].ToString().ToLower();

                DocumentFormat access = DocumentFormat.invalid;
                DocumentFormat input = DocumentFormat.invalid;
                DocumentFormat output = DocumentFormat.invalid;

                try
                {
                    if (string.IsNullOrEmpty(pRow[Convert.ToInt32(this.txtConfigColumnIndexAccessFormat.Text)].ToString()))
                    {
                        access = DocumentFormat.invalid;
                    }
                    else
                    {
                        access = (DocumentFormat)Enum.Parse(typeof(DocumentFormat), pRow[Convert.ToInt32(this.txtConfigColumnIndexAccessFormat.Text)].ToString().ToLower(), true);
                    }
                }
                catch (Exception)
                {
                    access = DocumentFormat.invalid;
                }

                try
                {
                    if (string.IsNullOrEmpty(pRow[Convert.ToInt32(this.txtConfigColumnIndexInputFormat.Text)].ToString()))
                    {
                        input = DocumentFormat.invalid;
                    }
                    else
                    {
                        input = (DocumentFormat)Enum.Parse(typeof(DocumentFormat), pRow[Convert.ToInt32(this.txtConfigColumnIndexInputFormat.Text)].ToString().ToLower(), true);
                    }
                }
                catch (Exception)
                {
                    input = DocumentFormat.invalid;
                }

                try
                {
                    if (string.IsNullOrEmpty(pRow[Convert.ToInt32(this.txtConfigColumnIndexOutputFormat.Text)].ToString()))
                    {
                        output = DocumentFormat.invalid;
                    }
                    else
                    {
                        output = (DocumentFormat)Enum.Parse(typeof(DocumentFormat), pRow[Convert.ToInt32(this.txtConfigColumnIndexOutputFormat.Text)].ToString().ToLower(), true);
                    }
                }
                catch (Exception)
                {
                    output = DocumentFormat.invalid;
                }

                string creator = pRow[Convert.ToInt32(this.txtConfigColumnIndexIsCreator.Text)].ToString().ToLower();
                string modifications = pRow[Convert.ToInt32(this.txtConfigColumnIndexIsModificator.Text)].ToString().ToLower();
                string editor = pRow[Convert.ToInt32(this.txtConfigColumnIndexIsEditor.Text)].ToString().ToLower();

                bool isCreator = false;
                bool isModificator = false;
                bool isEditor = false;
                
                if (this.applicableIndicatorsYes.Contains(creator))
                {
                    isCreator = true;
                }
                else if (this.applicableIndicatorsNo.Contains(creator))
                {
                    isCreator = false;
                }
                else
                {
                    isCreator = false;
                    this.worker.ReportProgress(0, "fehler: " + pDepartment + ";" + name + ";isCreator");
                }

                if (this.applicableIndicatorsYes.Contains(modifications))
                {
                    isModificator = true;
                }
                else if (this.applicableIndicatorsNo.Contains(modifications))
                {
                    isModificator = false;
                }
                else
                {
                    isModificator = false;
                    this.worker.ReportProgress(0, "fehler: " + pDepartment + ";" + name + ";isModificator");
                }

                if (this.applicableIndicatorsYes.Contains(editor))
                {
                    isEditor = true;
                }
                else if (this.applicableIndicatorsNo.Contains(editor))
                {
                    isEditor = false;
                }
                else
                {
                    isEditor = false;
                    this.worker.ReportProgress(0, "fehler: " + pDepartment + ";" + name + ";isEditor");
                }

                if (!string.IsNullOrEmpty(name))
                {
                        Document d = new Document(name, 
                        description,
                        notes,
                        improvements,
                        isCreator,
                        isModificator,
                        isEditor,
                        pDepartment, 
                        departmentSource, 
                        departmentDestination, 
                        access, 
                        input, 
                        output);


                    int applied = 0;

                    foreach(int index in this.trayCriteriaColumns)
                    {
                        string value = pRow[index].ToString().ToLower();

                        if (this.applicableIndicatorsYes.Contains(value))
                        {
                            applied++;
                        }
                    }

                    string frequency = pRow[Convert.ToInt32(this.txtConfigColumnIndexFrequency.Text)].ToString().ToLower();
                    FrequencyType frequencyType = FrequencyType.Low;
                    if(frequency.Equals("6 bis 19"))
                    {
                        frequencyType = FrequencyType.Medium;
                    }
                    else if(frequency.Equals("20 oder mehr"))
                    {
                        frequencyType = FrequencyType.High;
                    }
                    
                    List<string> nd = new List<string>();

                    if(!string.IsNullOrEmpty(pRow[Convert.ToInt32(this.txtConfigColumnIndexNotifiedDepartments.Text)].ToString()))
                    {
                        nd.AddRange(pRow[Convert.ToInt32(this.txtConfigColumnIndexNotifiedDepartments.Text)].ToString().ToLower().Split(';'));
                    }

                    d.setTrayCriteriaCount(applied);
                    d.setFrequencyCount(frequencyType);
                    d.setNotifiedDepartments(nd);
                    
                    this.documents.Add(d);
                }
                else
                {
                    this.worker.ReportProgress(0, "ungültige daten gefunden");
                }
            }
            catch(Exception ex)
            {
                this.worker.ReportProgress(0, ex.Message);
            }
        }

        public void fillAllDocumentsView()
        {
            if(this.documents != null && this.worker != null && !this.worker.IsBusy)
            {
                this.txtListAllDocuments.Items.Clear();

                List<Document> data = this.documents;

                if (this.sequence.Count > 0)
                {
                    data = this.sequence;
                }
                else if (this.top20.Count > 0)
                {
                    data = this.top20;
                }

                foreach (Document d in data)
                {
                    if(this.txtFilterByDepartment.SelectedIndex > 0 && !this.txtFilterByDepartment.SelectedItem.Equals(d.getDepartment()))
                    {
                        continue;
                    }

                    string result = "Reine Ablage";
                    bool integrated = false;
                    bool immediately = true;

                    ListViewItem lvi = new ListViewItem(d.getName());
                    lvi.UseItemStyleForSubItems = false;

                    lvi.SubItems.Add(d.getDescription());                    
                    lvi.SubItems.Add(d.isCreator().ToString());
                    lvi.SubItems.Add(d.isModificator().ToString());
                    lvi.SubItems.Add(d.isEditor().ToString());                    
                    lvi.SubItems.Add(d.getDepartment());

                    string sources = "";
                    string destinations = "";

                    foreach (string s in d.getDepartmentSource())
                    {
                        sources += s + ";";
                    }

                    foreach (string s in d.getDepartmentDestination())
                    {
                        destinations += s + ";";
                    }

                    if (!string.IsNullOrEmpty(sources))
                    {
                        sources = sources.Substring(0, sources.Length - 1);
                    }

                    if (!string.IsNullOrEmpty(destinations))
                    {
                        destinations = destinations.Substring(0, destinations.Length - 1);
                    }

                    lvi.SubItems.Add(sources);
                    lvi.SubItems.Add(destinations);


                    lvi.SubItems.Add(d.getDocumentAccess().ToString());
                    lvi.SubItems[lvi.SubItems.Count - 1].ForeColor = Color.Green;

                    if (d.getDocumentAccess().Equals(DocumentFormat.papier) || d.getDocumentAccess().Equals(DocumentFormat.invalid))
                    {
                        lvi.SubItems[lvi.SubItems.Count - 1].ForeColor = Color.Red;
                        immediately = false;
                    }
                    if (this.txtFilterByAccessFormat.SelectedIndex > 0 && !this.txtFilterByAccessFormat.SelectedItem.ToString().Equals(d.getDocumentAccess().ToString()))
                    {
                        continue;
                    }

                    lvi.SubItems.Add(d.getDocumentInput().ToString());
                    lvi.SubItems[lvi.SubItems.Count - 1].ForeColor = Color.Green;
                    if (d.getDocumentInput().Equals(DocumentFormat.papier) || d.getDocumentInput().Equals(DocumentFormat.invalid))
                    {
                        lvi.SubItems[lvi.SubItems.Count - 1].ForeColor = Color.Red;
                        //immediately = false;
                    }

                    if (this.txtFilterByInputFormat.SelectedIndex > 0 && !this.txtFilterByInputFormat.SelectedItem.ToString().Equals(d.getDocumentInput().ToString()))
                    {
                        continue;
                    }

                    lvi.SubItems.Add(d.getDocumentOutput().ToString());
                    lvi.SubItems[lvi.SubItems.Count - 1].ForeColor = Color.Green;
                    if (d.getDocumentOutput().Equals(DocumentFormat.papier) || d.getDocumentOutput().Equals(DocumentFormat.invalid))
                    {
                        lvi.SubItems[lvi.SubItems.Count - 1].ForeColor = Color.Red;
                        //immediately = false;
                    }

                    if (this.txtFilterByOutputFormat.SelectedIndex > 0 && !this.txtFilterByOutputFormat.SelectedItem.ToString().Equals(d.getDocumentOutput().ToString()))
                    {
                        continue;
                    }

                    string txt = d.getStatisfiedTrayCriteriaCount().ToString() + " / " + this.trayCriteriaColumns.Count;

                    lvi.SubItems.Add(txt);
                    lvi.SubItems[lvi.SubItems.Count - 1].ForeColor = Color.Red;

                    if (this.trayCriteriaColumns.Count == 0)
                    {
                        lvi.SubItems[lvi.SubItems.Count - 1].ForeColor = Color.Black;
                    }
                    else if (d.getStatisfiedTrayCriteriaCount() > 0)
                    {
                        lvi.SubItems[lvi.SubItems.Count - 1].ForeColor = Color.Green;
                        result = "Erweiterte Ablage";
                    }

                    bool add = true;

                    if (this.txtFilterByTrayCriteria.SelectedIndex == 1 && d.getStatisfiedTrayCriteriaCount() == 0 && this.trayCriteriaColumns.Count > 0)
                    {
                        add = false;
                    }
                    else if (this.txtFilterByTrayCriteria.SelectedIndex == 2 && d.getStatisfiedTrayCriteriaCount() > 0 && this.trayCriteriaColumns.Count > 0)
                    {
                        add = false;
                    }

                    lvi.SubItems.Add(d.getFrequency().ToString());
                    lvi.SubItems[lvi.SubItems.Count - 1].ForeColor = Color.Green;

                    if (d.getFrequency().Equals(FrequencyType.Low))
                    {
                        lvi.SubItems[lvi.SubItems.Count - 1].ForeColor = Color.Red;
                    }

                    lvi.SubItems.Add(d.getNotifiedDepartments().Count.ToString());
                    lvi.SubItems[lvi.SubItems.Count - 1].ForeColor = Color.Green;

                    if (d.getNotifiedDepartments().Count == 0)
                    {
                        lvi.SubItems[lvi.SubItems.Count - 1].ForeColor = Color.Red;
                    }
                    
                    if (this.txtFilterByWorkflowCriteria.SelectedIndex == 1 
                        && (d.getFrequency().Equals(FrequencyType.Low) && d.getNotifiedDepartments().Count == 0))
                    {
                        add = false;
                    }
                    else if (this.txtFilterByWorkflowCriteria.SelectedIndex == 2
                        && !(d.getFrequency().Equals(FrequencyType.Low) && d.getNotifiedDepartments().Count == 0))
                    {
                        add = false;
                    }

                    if (d.getFrequency() > 0 || d.getNotifiedDepartments().Count > 0)
                    {
                        result = "DMS würdig (momentan nicht umsetzbar)";

                        if (immediately)
                        {
                            result = "DMS würdig (ok)";
                        }
                    }

                    if (this.txtFilterByResult.SelectedIndex > 0 && !result.Equals(this.txtFilterByResult.SelectedItem.ToString()))
                    {
                        add = false;
                    }

                    if (add)
                    {
                        lvi.SubItems.Add(result);
                        lvi.SubItems.Add(integrated.ToString());
                        this.txtListAllDocuments.Items.Add(lvi);
                    }
                }

                this.tabControl1.TabPages[0].Text = "Dokumente (" + this.txtListAllDocuments.Items.Count + ")";
                this.txtListAllDocuments.Update();
            }
        }

        private void btStartDLA_Click(object sender, EventArgs e)
        {
            if (this.worker != null && !this.worker.IsBusy && !this.worker.CancellationPending)
            {
                this.oneSheet = false;

                if (MessageBox.Show("Ein Gesamtdokument verwenden?", "Config", MessageBoxButtons.YesNo).Equals(DialogResult.Yes))
                {
                    OpenFileDialog ofd = new OpenFileDialog();

                    if(ofd.ShowDialog().Equals(DialogResult.OK))
                    {
                        this.oneSheet = true;
                        this.dataDirectory = ofd.FileName;

                        if (File.Exists(this.dataDirectory))
                        {
                            this.clearUI();

                            ListViewItem item1 = new ListViewItem("Datei");
                            item1.SubItems.Add(this.dataDirectory);
                            this.txtInfoBox.Items.Add(item1);
                            this.txtProgress.Maximum = 1;
                            this.worker.RunWorkerAsync();
                            this.btStartDLA.Enabled = false;
                        }
                        else
                        {
                            this.logEvent("ungültiger Pfad");
                        }
                    }
                }
                else
                {
                    FolderBrowserDialog fbd = new FolderBrowserDialog();

                    if (fbd.ShowDialog().Equals(DialogResult.OK))
                    {
                        this.dataDirectory = fbd.SelectedPath;

                        if (Directory.Exists(this.dataDirectory))
                        {
                            this.clearUI();

                            ListViewItem item1 = new ListViewItem("Ordner");
                            item1.SubItems.Add(this.dataDirectory);
                            this.txtInfoBox.Items.Add(item1);

                            DirectoryInfo di = new DirectoryInfo(this.dataDirectory);
                            FileInfo[] files = di.GetFiles("*.xls*", SearchOption.TopDirectoryOnly);

                            this.txtProgress.Maximum = files.Length;

                            ListViewItem item2 = new ListViewItem("Dokumente");
                            item2.SubItems.Add(files.Length.ToString());
                            this.txtInfoBox.Items.Add(item2);

                            this.worker.RunWorkerAsync();
                            this.btStartDLA.Enabled = false;
                        }
                        else
                        {
                            this.logEvent("ungültiger Pfad");
                        }
                    }
                }
            }
            else
            {
                this.logEvent("Operation nicht erlaubt");
            }
        }

        private void btStop_Click(object sender, EventArgs e)
        {
            if(this.worker != null && this.worker.IsBusy && !this.worker.CancellationPending)
            {
                this.worker.CancelAsync();
                this.btStop.Enabled = false;
            }
            else
            {
                this.logEvent("Operation nicht erlaubt");
            }
        }

        private void txtFilterDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.top20 = new List<Document>();

            this.fillAllDocumentsView();
        }

        private void txtLogicalFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.top20 = new List<Document>();

            this.fillAllDocumentsView();
        }

        private void txtAccessFormatFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.top20 = new List<Document>();

            this.fillAllDocumentsView();
        }

        private void txtInputFormatFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.top20 = new List<Document>();

            this.fillAllDocumentsView();
        }

        private void txtOutputFormatFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.top20 = new List<Document>();
            this.fillAllDocumentsView();
        }

        private void btSaveReport_Click(object sender, EventArgs e)
        {
            List<int> indicies = new List<int>();
            indicies.Add(0);
            indicies.Add(5);
            indicies.Add(8);
            indicies.Add(11);
            indicies.Add(12);
            indicies.Add(13);
            indicies.Add(14);
            
            Util.exportPDF("DLA Report - Document Analysis", this.txtListAllDocuments, indicies);
        }

        private void identifyWorkflows()
        {
            if(this.worker != null)
            {
                this.workflows.Clear();

                Dictionary<string, List<Document>> tmp = new Dictionary<string, List<Document>>();

                foreach(Document d in this.documents)
                {
                    if(!tmp.ContainsKey(d.getName()))
                    {
                        tmp.Add(d.getName(), new List<Document>());
                    }

                    tmp[d.getName()].Add(d);                    
                }

                foreach(List<Document> list in tmp.Values)
                {
                    if(list.Count > 1)
                    {
                        string info = "workflow <" + list[0].getName() + ">:";
                        foreach(Document d in list)
                        {
                            info += " " + d.getDepartment();
                        }

                        this.worker.ReportProgress(0, info);
                        this.workflows.Add(list[0].getName(), list);
                    }
                }
            }
        }

        private void identifyProblems()
        {
            if (this.worker != null)
            {
                this.txtProblemList.Items.Clear();
                Dictionary<string, List<List<string>>> missingDocumentsByDepartmnent = new Dictionary<string, List<List<string>>>();

                foreach (Document document in this.documents)
                {
                    if(document.getDocumentInput().Equals(DocumentFormat.invalid) && document.getDepartmentSource().Count > 0)
                    {
                        ListViewItem item = new ListViewItem((this.txtProblemList.Items.Count + 1).ToString());
                        item.SubItems.Add(document.getName());
                        item.SubItems.Add(document.getDepartment());
                        item.SubItems.Add("Dokument besitzt Quellabteilung, hat aber kein Input-Format spezifiziert");
                        this.txtProblemList.Items.Add(item);
                    }

                    if (document.getDocumentOutput().Equals(DocumentFormat.invalid) && document.getDepartmentDestination().Count > 0)
                    {
                        ListViewItem item = new ListViewItem((this.txtProblemList.Items.Count + 1).ToString());
                        item.SubItems.Add(document.getName());
                        item.SubItems.Add(document.getDepartment());
                        item.SubItems.Add("Dokument besitzt Zielabteilung, hat aber kein Output-Format spezifiziert");
                        this.txtProblemList.Items.Add(item);
                    }

                    if (!document.getDocumentInput().Equals(DocumentFormat.invalid) && document.getDepartmentSource().Count == 0)
                    {
                        ListViewItem item = new ListViewItem((this.txtProblemList.Items.Count + 1).ToString());
                        item.SubItems.Add(document.getName());
                        item.SubItems.Add(document.getDepartment());
                        item.SubItems.Add("Dokument spezifiziert Input-Format, hat aber keine Quellabteilung angegeben");
                        this.txtProblemList.Items.Add(item);
                    }

                    if (!document.getDocumentOutput().Equals(DocumentFormat.invalid) && document.getDepartmentDestination().Count == 0)
                    {
                        ListViewItem item = new ListViewItem((this.txtProblemList.Items.Count + 1).ToString());
                        item.SubItems.Add(document.getName());
                        item.SubItems.Add(document.getDepartment());
                        item.SubItems.Add("Dokument spezifiziert Output-Format, hat aber keine Zielabteilung angegeben");
                        this.txtProblemList.Items.Add(item);
                    }

                    List<Document> workflowData = new List<Document>();
                    
                    if (this.workflows.ContainsKey(document.getName()))
                    {
                        workflowData = this.workflows[document.getName()];
                    }
                    
                    // verify source department
                    if(document.getDepartmentSource().Count > 0)
                    {
                        foreach (string source in document.getDepartmentSource())
                        {
                            bool foundSourceDepartment = false;

                            foreach (Document d in workflowData)
                            {
                                if (d.getDepartment().Equals(source))
                                {
                                    foundSourceDepartment = true;
                                    break;
                                }
                            }

                            if (!foundSourceDepartment && !source.Equals(document.getDepartment()))
                            {
                                ListViewItem item = new ListViewItem((this.txtProblemList.Items.Count + 1).ToString());
                                item.SubItems.Add(document.getName());
                                item.SubItems.Add(document.getDepartment());
                                item.SubItems.Add("hat Quellabteilung <" + source + "> angegeben; ist aber nicht in Referenzdaten angegeben");

                                if (!missingDocumentsByDepartmnent.ContainsKey(source))
                                {
                                    missingDocumentsByDepartmnent.Add(source, new List<List<string>>());
                                }
                                
                                List<string> list = new List<string>();
                                list.Add(document.getName());
                                list.Add(document.getDepartment());
                                list.Add("Quellabteilung");
                                missingDocumentsByDepartmnent[source].Add(list);

                                //if (!missingDocumentsByDepartmnent[source].Contains(document.getName()))
                                //{
                                //    missingDocumentsByDepartmnent[source].Add(document.getName());
                                //}

                                this.txtProblemList.Items.Add(item);
                            }
                        }
                    }

                    // verify destination department
                    if (document.getDepartmentDestination().Count > 0)
                    {
                        foreach (string destination in document.getDepartmentDestination())
                        {
                            bool foundDestinationDepartment = false;

                            foreach (Document d in workflowData)
                            {
                                if (d.getDepartment().Equals(destination))
                                {
                                    foundDestinationDepartment = true;
                                    break;
                                }
                            }

                            if (!foundDestinationDepartment && !destination.Equals(document.getDepartment()))
                            {
                                ListViewItem item = new ListViewItem((this.txtProblemList.Items.Count + 1).ToString());
                                item.SubItems.Add(document.getName());
                                item.SubItems.Add(document.getDepartment());
                                item.SubItems.Add("hat Zielabteilung <" + destination + "> angegeben; ist aber nicht in Referenzdaten angegeben");

                                if (!missingDocumentsByDepartmnent.ContainsKey(destination))
                                {
                                    missingDocumentsByDepartmnent.Add(destination, new List<List<string>>());
                                }

                                List<string> list = new List<string>();
                                list.Add(document.getName());
                                list.Add(document.getDepartment());
                                list.Add("Zielabteilung");
                                missingDocumentsByDepartmnent[destination].Add(list);
                                
                                //if (!missingDocumentsByDepartmnent[destination].Contains(document.getName()))
                                //{
                                //    missingDocumentsByDepartmnent[destination].Add(document.getName());
                                //}

                                this.txtProblemList.Items.Add(item);
                            }
                        }
                    }
                }

                foreach (string department in missingDocumentsByDepartmnent.Keys)
                {
                    this.worker.ReportProgress(0, ";" + department + ";" + missingDocumentsByDepartmnent[department].Count);

                    foreach (List<string> documentData in missingDocumentsByDepartmnent[department])
                    {
                        if(department.Equals(documentData[1]))
                        {
                            continue;
                        }

                        string data = "";
                        foreach(string s in documentData)
                        {
                            data += ";" + s;
                        }

                        this.worker.ReportProgress(0, data);
                    }
                }
            }
        }

        public void fillWorkflowSequence(List<Document> pSequence, string pWorkflowName)
        {
            this.currentWorkflowName = pWorkflowName;
            this.sequence = pSequence;
            this.fillAllDocumentsView();
        }
        
        public void fillIdentifiedWorkflowsSelector()
        {
            if(this.workflows != null && this.workflows.Count > 0)
            {
                this.txtWorkflowSelector.Items.Clear();
                this.txtWorkflowSelector.Items.Add("---");

                foreach(string s in this.workflows.Keys)
                {
                    this.txtWorkflowSelector.Items.Add(s);
                }
            }
        }

        private void btOpenWorkflowAnalyzer_Click(object sender, EventArgs e)
        {
            if (this.documents != null && this.documents.Count > 0)
            {
                MainView view = this;
                new WorkflowAnalyzerView(ref view, this.documents);
            }
            else
            {
                MessageBox.Show("keine Daten vorhanden");
            }
        }

        private void txtWorkflowSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.top20 = new List<Document>();

            if(this.txtWorkflowSelector.SelectedIndex == 0)
            {
                this.sequence = new List<Document>();
                this.fillAllDocumentsView();
            }
            else if (this.workflows != null && this.workflows.Count > 0 && this.workflows.ContainsKey(this.txtWorkflowSelector.SelectedItem.ToString()))
            {
                this.sequence = this.workflows[this.txtWorkflowSelector.SelectedItem.ToString()];
                this.fillAllDocumentsView();
            }            
        }

        private void btExportProblemReport_Click(object sender, EventArgs e)
        {
            Util.exportPDF("DLA Report - Problem Report", this.txtProblemList, new List<int>());
        }
     
        private void btViewWorkflow_Click(object sender, EventArgs e)
        {
            if (this.sequence.Count > 0)
            {
                Dictionary<string, Document> data = new Dictionary<string, Document>();

                foreach (Document d in this.sequence)
                {
                    data.Add(d.getDepartment(), d);
                }

                new WorkflowFlowView(data).Show();
            }
            else
            {
                MessageBox.Show("Bitte zunächst Workflow wählen");
            }
        }

        private void btShowTop20_Click(object sender, EventArgs e)
        {
            List<Document> tmp = new List<Document>();

            foreach(Document d in this.documents)
            {
                if(d.getFrequency().Equals(FrequencyType.High))
                {
                    tmp.Add(d);
                }
            }

            this.top20 = tmp.OrderBy(o => o.getNotifiedDepartments().Count).ToList();
            this.top20.Reverse();
          
            if(this.top20.Count > 20)
            {
                this.top20.RemoveRange(19, this.top20.Count - 19);
            }

            this.fillAllDocumentsView();
        }

        private void btExportAllWorkflows_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fdb = new FolderBrowserDialog();

            if (fdb.ShowDialog().Equals(DialogResult.OK))
            {
                foreach(string key in this.workflows.Keys)
                {
                    Dictionary<string, Document> data = new Dictionary<string, Document>();

                    foreach (Document d in this.workflows[key])
                    {
                        data.Add(d.getDepartment(), d);
                    }

                    WorkflowFlowView wfv = new WorkflowFlowView(data);
                    wfv.WindowState = FormWindowState.Maximized;
                    wfv.Show();
                    wfv.silentExport(fdb.SelectedPath);
                    wfv.Close();
                }

                MessageBox.Show("Export erfolgreich");
            }
        }
    }
}