﻿namespace DLA
{
    partial class WorkflowAnalyzerView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btAnalyze = new System.Windows.Forms.Button();
            this.btRemoveLast = new System.Windows.Forms.Button();
            this.btAddDocument = new System.Windows.Forms.Button();
            this.txtAllDocuments = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtWorkflowSequence = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.txtWorkflowName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 454);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(970, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Size = new System.Drawing.Size(970, 454);
            this.splitContainer1.SplitterDistance = 323;
            this.splitContainer1.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtWorkflowName);
            this.groupBox1.Controls.Add(this.btAnalyze);
            this.groupBox1.Controls.Add(this.btRemoveLast);
            this.groupBox1.Controls.Add(this.btAddDocument);
            this.groupBox1.Controls.Add(this.txtAllDocuments);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(323, 454);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Available Documents";
            // 
            // btAnalyze
            // 
            this.btAnalyze.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btAnalyze.Location = new System.Drawing.Point(3, 428);
            this.btAnalyze.Name = "btAnalyze";
            this.btAnalyze.Size = new System.Drawing.Size(317, 23);
            this.btAnalyze.TabIndex = 3;
            this.btAnalyze.Text = "Start Analyzer";
            this.btAnalyze.UseVisualStyleBackColor = true;
            this.btAnalyze.Click += new System.EventHandler(this.btAnalyze_Click);
            // 
            // btRemoveLast
            // 
            this.btRemoveLast.Dock = System.Windows.Forms.DockStyle.Top;
            this.btRemoveLast.Location = new System.Drawing.Point(3, 60);
            this.btRemoveLast.Name = "btRemoveLast";
            this.btRemoveLast.Size = new System.Drawing.Size(317, 23);
            this.btRemoveLast.TabIndex = 2;
            this.btRemoveLast.Text = "Remove Last";
            this.btRemoveLast.UseVisualStyleBackColor = true;
            this.btRemoveLast.Click += new System.EventHandler(this.btRemoveLast_Click);
            // 
            // btAddDocument
            // 
            this.btAddDocument.Dock = System.Windows.Forms.DockStyle.Top;
            this.btAddDocument.Location = new System.Drawing.Point(3, 37);
            this.btAddDocument.Name = "btAddDocument";
            this.btAddDocument.Size = new System.Drawing.Size(317, 23);
            this.btAddDocument.TabIndex = 1;
            this.btAddDocument.Text = "Add to End";
            this.btAddDocument.UseVisualStyleBackColor = true;
            this.btAddDocument.Click += new System.EventHandler(this.btAddDocument_Click);
            // 
            // txtAllDocuments
            // 
            this.txtAllDocuments.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtAllDocuments.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtAllDocuments.FormattingEnabled = true;
            this.txtAllDocuments.Location = new System.Drawing.Point(3, 16);
            this.txtAllDocuments.Name = "txtAllDocuments";
            this.txtAllDocuments.Size = new System.Drawing.Size(317, 21);
            this.txtAllDocuments.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtWorkflowSequence);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(643, 454);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Workflow Sequence";
            // 
            // txtWorkflowSequence
            // 
            this.txtWorkflowSequence.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.txtWorkflowSequence.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtWorkflowSequence.Location = new System.Drawing.Point(3, 16);
            this.txtWorkflowSequence.Name = "txtWorkflowSequence";
            this.txtWorkflowSequence.Size = new System.Drawing.Size(637, 435);
            this.txtWorkflowSequence.TabIndex = 0;
            this.txtWorkflowSequence.UseCompatibleStateImageBehavior = false;
            this.txtWorkflowSequence.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "#";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Document";
            this.columnHeader2.Width = 200;
            // 
            // txtWorkflowName
            // 
            this.txtWorkflowName.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtWorkflowName.Location = new System.Drawing.Point(3, 408);
            this.txtWorkflowName.Name = "txtWorkflowName";
            this.txtWorkflowName.Size = new System.Drawing.Size(317, 20);
            this.txtWorkflowName.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Location = new System.Drawing.Point(3, 395);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Workflow Name";
            // 
            // WorkflowAnalyzerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(970, 476);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "WorkflowAnalyzerView";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Workflow Analyzer";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btAnalyze;
        private System.Windows.Forms.Button btRemoveLast;
        private System.Windows.Forms.Button btAddDocument;
        private System.Windows.Forms.ComboBox txtAllDocuments;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView txtWorkflowSequence;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtWorkflowName;
    }
}