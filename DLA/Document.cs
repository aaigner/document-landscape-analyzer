﻿using System;
using System.Collections.Generic;

namespace DLA
{  
    public class Document
    {
        private const char DEPARTMENT_SPLITTER = ';';

        private string name;
        private string description;
        private string department;
        private List<string> departmentSource;
        private List<string> departmentDestination;
        private DocumentFormat documentInput;
        private DocumentFormat documentAccess;
        private DocumentFormat documentOutput;
        private bool creator;
        private bool modificator;
        private bool editor;
        private string note;
        private string improvements;

        private int satisfiedTrayCriteria;
        private FrequencyType frequencyCount;
        private List<string> notifiedDepartments;

        public Document(string pName, string pDescription, string pNotes, string pImprovements,
            bool pCreator, bool pModificator, bool pEditor, 
            string pDepartment, string pDepartmentSource, string pDepartmentDestination, 
            DocumentFormat pDocumentAccess, DocumentFormat pDocumentInput, DocumentFormat pDocumentOutput)
        {
            this.name = pName;
            this.description = pDescription;
            this.creator = pCreator;
            this.modificator = pModificator;
            this.editor = pEditor;
            this.department = pDepartment;
            this.note = pNotes;
            this.improvements = pImprovements;

            this.departmentSource = new List<string>();
            this.departmentDestination = new List<string>();
            this.notifiedDepartments = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(pDepartmentSource))
                {
                    string[] raw = pDepartmentSource.Split(DEPARTMENT_SPLITTER);

                    if (raw != null & raw.Length > 0)
                    {
                        foreach (string dep in raw)
                        {
                            if (dep == null || string.IsNullOrEmpty(dep))
                            {
                                break;
                            }

                            this.departmentSource.Add(dep.Trim());
                        }
                    }
                }

                if (!string.IsNullOrEmpty(pDepartmentDestination))
                {
                    string[] raw = pDepartmentDestination.Split(DEPARTMENT_SPLITTER);

                    if (raw != null & raw.Length > 0)
                    {
                        foreach (string dep in pDepartmentDestination.Split(DEPARTMENT_SPLITTER))
                        {
                            if (dep == null || string.IsNullOrEmpty(dep))
                            {
                                break;
                            }

                            this.departmentDestination.Add(dep.Trim());
                        }
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            this.documentAccess = pDocumentAccess;
            this.documentInput = pDocumentInput;
            this.documentOutput = pDocumentOutput;
        }

        public void setTrayCriteriaCount(int pCount)
        {
            this.satisfiedTrayCriteria = pCount;
        }

        public void setFrequencyCount(FrequencyType pFrequencyType)
        {
            this.frequencyCount = pFrequencyType;
        }

        public void setNotifiedDepartments(List<string> pDepartments)
        {
            this.notifiedDepartments.AddRange(pDepartments);
        }

        public int getStatisfiedTrayCriteriaCount()
        {
            return this.satisfiedTrayCriteria;
        }

        public FrequencyType getFrequency()
        {
            return this.frequencyCount;
        }

        public List<string> getNotifiedDepartments()
        {
            return this.notifiedDepartments;
        }

        public string getName()
        {
            return this.name;
        }

        public string getDescription()
        {
            return this.description;
        }

        public string getDepartment()
        {
            return this.department;
        }

        public bool isCreator()
        {
            return this.creator;
        }

        public bool isModificator()
        {
            return this.modificator;
        }

        public bool isEditor()
        {
            return this.editor;
        }

        public List<string> getDepartmentSource()
        {
            return this.departmentSource;
        }

        public List<string> getDepartmentDestination()
        {
            return this.departmentDestination;
        }

        public DocumentFormat getDocumentAccess()
        {
            return this.documentAccess;
        }

        public DocumentFormat getDocumentInput()
        {
            return this.documentInput;
        }

        public DocumentFormat getDocumentOutput()
        {
            return this.documentOutput;
        }

        public void addDestination(string pDep)
        {
            this.departmentDestination.Add(pDep);
        }

        public void addSource(string pDep)
        {
            this.departmentSource.Add(pDep);
        }
    }
}
