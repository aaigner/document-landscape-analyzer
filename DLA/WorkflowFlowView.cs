﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace DLA
{
    public partial class WorkflowFlowView : Form
    {
        private string caption;

        public WorkflowFlowView(Dictionary<string, Document> pData)
        {
            InitializeComponent();
            
            this.panel.AutoScroll = true;
            this.panel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.panel.HorizontalScroll.Enabled = true;
            this.panel.HorizontalScroll.Visible = true;

            Dictionary<string, Document> newData = new Dictionary<string, Document>();

            foreach (Document d in pData.Values)
            {
                this.Text = "Workflow Viewer - " + d.getName().ToUpper();
                this.caption = d.getName().ToUpper();

                foreach (string s in d.getDepartmentSource())
                {
                    if (pData.ContainsKey(s))
                    {
                        // ok
                    }
                    else if (newData.ContainsKey(s))
                    {
                        newData[s].addDestination(d.getDepartment());
                    }
                    else
                    {
                        Document doc = new Document(d.getName(), "", "", "", false, false, false, s, "", d.getDepartment(), DocumentFormat.invalid, DocumentFormat.invalid, DocumentFormat.invalid);
                        newData.Add(s, doc);
                    }
                }

                foreach (string s in d.getDepartmentDestination())
                {
                    if (pData.ContainsKey(s))
                    {
                        // ok
                    }
                    else if (newData.ContainsKey(s))
                    {
                        newData[s].addSource(d.getDepartment());
                    }
                    else
                    {
                        Document doc = new Document(d.getName(), "", "", "", false, false, false, s, d.getDepartment(), "", DocumentFormat.invalid, DocumentFormat.invalid, DocumentFormat.invalid);
                        newData.Add(s, doc);
                    }
                }
            }

            foreach (string s in newData.Keys)
            {
                pData.Add(s, newData[s]);
            }

            this.generateWorkflow(pData);
        }

        private void btExportGraphic_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            if (fbd.ShowDialog().Equals(DialogResult.OK))
            {
                try
                {
                    string basePath = fbd.SelectedPath + Path.DirectorySeparatorChar + Util.getTimestamp() + "-workflow - " + this.caption;
                    int i = 0;
                    List<Bitmap> screens = new List<Bitmap>();

                    foreach (Control c in this.panel.Controls)
                    {
                        if (c is FlowLayoutPanel)
                        {
                            Bitmap bm = new Bitmap(c.ClientSize.Width, c.ClientSize.Height);
                            FlowLayoutPanel flp = (FlowLayoutPanel)c;
                            flp.DrawToBitmap(bm, flp.ClientRectangle);
                            screens.Add(bm);
                            string tmp = basePath + "img" + i + ".jpg";
                            i++;
                            bm.Save(tmp);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void addAltertEntry(string pDepartment, string pCategory, string pAlert, string pDMSIndicator)
        {
            ListViewItem item = new ListViewItem((this.txtAlertList.Items.Count + 1).ToString());
            item.SubItems.Add(pDepartment);
            item.SubItems.Add(pCategory);
            item.SubItems.Add(pAlert);
            item.SubItems.Add(pDMSIndicator);

            this.txtAlertList.Items.Add(item);
        }

        private void btExportReport_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            if (fbd.ShowDialog().Equals(DialogResult.OK))
            {
                try
                {
                    string path = fbd.SelectedPath + Path.DirectorySeparatorChar + Util.getTimestamp() + "-workflow-" + this.caption + ".pdf";

                    using (FileStream stream = new FileStream(path, FileMode.Create))
                    {
                        iTextSharp.text.Document pdf = new iTextSharp.text.Document(PageSize.A4, 10f, 10f, 10f, 0f);

                        PdfWriter.GetInstance(pdf, stream);
                        pdf.Open();
                        pdf.Add(new Paragraph("Workflow Details für " + this.caption.ToUpper()));
                        pdf.Add(Util.listViewToPDFTable(this.txtAlertList, new List<int>()));
                        pdf.Close();
                        stream.Close();
                    }

                    MessageBox.Show("Saved report: " + path);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
        }

        private void addBox(ref FlowLayoutPanel pPanel, Document pDocument)
        {
            Panel panel = new Panel();
            panel.Width = 300;
            panel.Height = 110;
            panel.Padding = new Padding(20);
            panel.Margin = new Padding(20);
            panel.ForeColor = Color.Black;
            panel.BackColor = Color.DarkGray;

            string mode = "lesen";

            if (pDocument.isEditor())
            {
                panel.BackColor = Color.DarkBlue;
                panel.ForeColor = Color.White;

                mode += " | bearbeiten";
            }

            if (pDocument.isCreator())
            {
                panel.BackColor = Color.DarkGreen;
                panel.ForeColor = Color.White;

                mode += " | erstellen";

            }

            Label header = new Label();
            header.Text = pDocument.getDepartment();
            header.TextAlign = ContentAlignment.TopCenter;
            header.ForeColor = Color.White;
            header.Font = new System.Drawing.Font(FontFamily.GenericMonospace, 13);
            header.Dock = DockStyle.Top;

            Label access = new Label();
            access.Text = mode + " [" + pDocument.getDocumentAccess().ToString() + "]";
            access.TextAlign = ContentAlignment.TopLeft;
            access.ForeColor = Color.White;
            access.Font = new System.Drawing.Font(FontFamily.GenericSansSerif, 10);
            access.Dock = DockStyle.Top;

            Label dms = new Label();
            dms.Text = "Verwendungshäufigkeit:" + pDocument.getFrequency().ToString() + " | Benachrichtigungen: " + pDocument.getNotifiedDepartments().Count.ToString();
            dms.TextAlign = ContentAlignment.TopLeft;
            dms.ForeColor = Color.White;
            dms.Font = new System.Drawing.Font(FontFamily.GenericSansSerif, 10);
            dms.Dock = DockStyle.Top;

            bool tray = (pDocument.getStatisfiedTrayCriteriaCount() > 0) ? true : false;

            Label info = new Label();
            info.Text = "Ablagekriterien: " + tray.ToString();
            info.TextAlign = ContentAlignment.TopLeft;
            info.ForeColor = Color.White;
            info.Font = new System.Drawing.Font(FontFamily.GenericSansSerif, 10);
            info.Dock = DockStyle.Top;

            panel.Controls.Add(info);
            panel.Controls.Add(dms);
            panel.Controls.Add(access);
            panel.Controls.Add(header);

            pPanel.Controls.Add(panel);
        }

        public void addEdge(ref FlowLayoutPanel pPanel, string pLabel, bool pRight, bool pLeft)
        {
            Label l = new Label();
            l.Font = new System.Drawing.Font(FontFamily.GenericMonospace, 10);
            l.Width = 150;

            if (pLeft)
            {
                l.Text += "<";
            }

            l.Text += "--- " + pLabel + " ---";

            if (pRight)
            {
                l.Text += ">";
            }

            l.ForeColor = Color.DarkGreen;

            if (pLabel.ToLower().Equals(DocumentFormat.papier.ToString().ToLower()))
            {
                l.ForeColor = Color.DarkRed;
            }
            else if (pLabel.ToLower().Equals(DocumentFormat.invalid.ToString().ToLower()))
            {
                l.ForeColor = Color.Black;
            }

            pPanel.Controls.Add(l);            
            pPanel.Controls[pPanel.Controls.Count - 1].Anchor = AnchorStyles.None;
        }

        private void generateWorkflow(Dictionary<string, Document> pData)
        {
            try
            {
                List<Connection> connections = new List<Connection>();
                List<string> alreadyDone = new List<string>();
                List<List<Connection>> paths = new List<List<Connection>>();

                foreach (Document d in pData.Values)
                {
                    foreach (string source in d.getDepartmentSource())
                    {
                        if (d.getDepartment().Equals(source))
                        {
                            continue;
                        }

                        if (!alreadyDone.Contains(source + d.getDepartment()))
                        {
                            if (alreadyDone.Contains(d.getDepartment() + source))
                            {
                                foreach (Connection c in connections)
                                {
                                    if (c.source.Equals(d.getDepartment()) && c.destination.Equals(source))
                                    {
                                        c.bidirectional = true;
                                        break;
                                    }
                                }
                            }
                            else if (pData[source].getDepartmentDestination().Contains(d.getDepartment()))
                            {
                                connections.Add(new Connection(source, d.getDepartment()));
                                alreadyDone.Add(source + d.getDepartment());
                            }
                            else
                            {
                                if (!d.getDepartment().Equals(source))
                                {
                                    this.addAltertEntry(d.getDepartment(), "Ref Fehler", "gibt Quellabteilung " + source + " an, ist aber in Ref Daten nicht vorhanden", "");
                                }
                            }
                        }
                    }

                    foreach (string destination in d.getDepartmentDestination())
                    {
                        if (d.getDepartment().Equals(destination))
                        {
                            continue;
                        }

                        if (!alreadyDone.Contains(d.getDepartment() + destination))
                        {
                            if (alreadyDone.Contains(destination + d.getDepartment()))
                            {
                                foreach (Connection c in connections)
                                {
                                    if (c.source.Equals(destination) && c.destination.Equals(d.getDepartment()))
                                    {
                                        c.bidirectional = true;
                                        break;
                                    }
                                }
                            }
                            else if (pData[destination].getDepartmentSource().Contains(d.getDepartment()))
                            {
                                connections.Add(new Connection(d.getDepartment(), destination));
                                alreadyDone.Add(d.getDepartment() + destination);
                            }
                            else
                            {
                                if (!d.getDepartment().Equals(destination))
                                {
                                    this.addAltertEntry(d.getDepartment(), "Ref Fehler", "gibt Zielabteilung " + destination + " an, ist aber in Ref Daten nicht vorhanden", "");
                                }
                            }
                        }
                    }
                }

                while (connections.Count > 0)
                {
                    List<Connection> path = new List<Connection>();
                    path.Add(connections[0]);
                    connections.RemoveAt(0);
                    bool foundNew = true;

                    while (foundNew)
                    {
                        foundNew = false;

                        foreach (Connection c in connections)
                        {
                            if (path[0].source.Equals(c.destination))
                            {
                                List<Connection> tmp = new List<Connection>();
                                tmp.Add(c);
                                tmp.AddRange(path);
                                path = tmp;
                                foundNew = true;
                            }
                            else if (path[path.Count - 1].destination.Equals(c.source))
                            {
                                path.Add(c);
                                foundNew = true;
                            }
                        }

                        foreach (Connection c in path)
                        {
                            if (connections.Contains(c))
                            {
                                connections.Remove(c);
                            }
                        }
                    }

                    paths.Add(path);
                }

                List<Connection> masterList = new List<Connection>();
                masterList = paths[0];

                foreach (List<Connection> l in paths)
                {
                    if (l.Count > masterList.Count)
                    {
                        masterList = l;
                    }
                }

                paths.Remove(masterList);

                foreach (List<Connection> conn in paths)
                {
                    makeFlowLayout(pData, conn);
                }

                makeFlowLayout(pData, masterList);
            }
            catch(Exception)
            {

            }
        }

        private string determineDMSIndicator(Document pDoc)
        {
            string dms = "nicht geeignet";

            if (pDoc.getFrequency().Equals(FrequencyType.High) || pDoc.getNotifiedDepartments().Count > 0)
            {
                dms = "geeignet";
            }

            return dms;
        }

        private void makeFlowLayout(Dictionary<string, Document> pData, List<Connection> conn)
        {
            FlowLayoutPanel flp = new FlowLayoutPanel();
            flp.AutoScroll = true;
            flp.Height = 175;
            flp.WrapContents = false;
            flp.HorizontalScroll.Enabled = true;
            flp.HorizontalScroll.Visible = true;
            flp.BorderStyle = BorderStyle.FixedSingle;
            flp.Dock = DockStyle.Top;

            string lastDepartment = "";

            foreach (Connection c in conn)
            {
                Document source = pData[c.source];
                Document destination = pData[c.destination];

                string dmsSource = determineDMSIndicator(source);
                string dmsDestination = determineDMSIndicator(destination);

                if (!lastDepartment.Equals(c.source))
                {
                    if (source.getDocumentOutput().Equals(DocumentFormat.papier))
                    {
                        this.addAltertEntry(source.getDepartment(), "Output Format", "papier form nach " + destination.getDepartment(), dmsSource);
                    }

                    if (source.getDocumentAccess().Equals(DocumentFormat.papier))
                    {
                        this.addAltertEntry(source.getDepartment(), "Format", "bearbeitet dokument in papier form", dmsSource);
                    }

                    if (!source.getDocumentOutput().Equals(destination.getDocumentInput()))
                    {
                        this.addAltertEntry(source.getDepartment(), "Mismatch", "format konflikt mit " + destination.getDepartment(), dmsSource);
                    }

                    addBox(ref flp, source);
                }

                addEdge(ref flp, source.getDocumentOutput().ToString(), true, c.bidirectional);
                
                if (destination.getDocumentInput().Equals(DocumentFormat.papier))
                {
                    this.addAltertEntry(destination.getDepartment(), "Input Format", "erhält papier form von " + source.getDepartment(), dmsDestination);
                }

                if (destination.getDocumentAccess().Equals(DocumentFormat.papier))
                {
                    this.addAltertEntry(destination.getDepartment(), "Format", "bearbietet dokument in papier form ", dmsDestination);
                }

                if (!destination.getDocumentInput().Equals(source.getDocumentOutput()))
                {
                    this.addAltertEntry(destination.getDepartment(), "Mismatch", "format konflikt mit " + source.getDepartment(), dmsDestination);
                }

                addBox(ref flp, destination);

                lastDepartment = c.destination;
            }

            this.panel.Controls.Add(flp);
        }

        private void btExportFullReport_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            if (fbd.ShowDialog().Equals(DialogResult.OK))
            {
                try
                {
                    string path = fbd.SelectedPath + Path.DirectorySeparatorChar + Util.getTimestamp() + "-workflow-" + this.caption + ".pdf";
                    List<Bitmap> screens = new List<Bitmap>();

                    foreach (Control c in this.panel.Controls)
                    {
                        if (c is FlowLayoutPanel)
                        {
                            Bitmap bm = new Bitmap(c.ClientSize.Width, c.ClientSize.Height);
                            FlowLayoutPanel flp = (FlowLayoutPanel)c;
                            flp.DrawToBitmap(bm, flp.ClientRectangle);
                            screens.Add(bm);
                        }
                    }

                    using (FileStream stream = new FileStream(path, FileMode.Create))
                    {
                        iTextSharp.text.Document pdf = new iTextSharp.text.Document(PageSize.A4, 10f, 10f, 10f, 0f);
                        PdfWriter.GetInstance(pdf, stream);
                        pdf.Open();
                        pdf.Add(new Paragraph("Workflow Details für " + this.caption.ToUpper()));

                        foreach (Bitmap bm in screens)
                        {
                            System.Drawing.Image ptr = System.Drawing.Image.FromHbitmap(bm.GetHbitmap());
                            iTextSharp.text.Image i = iTextSharp.text.Image.GetInstance(ptr, BaseColor.WHITE);

                            i.ScalePercent(25f);
                            pdf.Add(i);
                        }

                        pdf.Add(Util.listViewToPDFTable(this.txtAlertList, new List<int>()));
                        pdf.Close();
                        stream.Close();
                    }

                    MessageBox.Show("Saved report: " + path);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        
        public void silentExport(string pFolder)
        {
            try
            {
                string path = pFolder + Path.DirectorySeparatorChar + Util.getTimestamp() + "-workflow-" + this.caption + ".pdf";
                List<Bitmap> screens = new List<Bitmap>();

                foreach (Control c in this.panel.Controls)
                {
                    if (c is FlowLayoutPanel)
                    {
                        Bitmap bm = new Bitmap(c.ClientSize.Width, c.ClientSize.Height);
                        FlowLayoutPanel flp = (FlowLayoutPanel)c;
                        flp.DrawToBitmap(bm, flp.ClientRectangle);
                        screens.Add(bm);
                    }
                }

                using (FileStream stream = new FileStream(path, FileMode.Create))
                {
                    iTextSharp.text.Document pdf = new iTextSharp.text.Document(PageSize.A4, 10f, 10f, 10f, 0f);
                    PdfWriter.GetInstance(pdf, stream);
                    pdf.Open();
                    pdf.Add(new Paragraph("Workflow Details für " + this.caption.ToUpper()));

                    foreach (Bitmap bm in screens)
                    {
                        System.Drawing.Image ptr = System.Drawing.Image.FromHbitmap(bm.GetHbitmap());
                        iTextSharp.text.Image i = iTextSharp.text.Image.GetInstance(ptr, BaseColor.WHITE);

                        i.ScalePercent(25f);
                        pdf.Add(i);
                    }

                    pdf.Add(Util.listViewToPDFTable(this.txtAlertList, new List<int>()));
                    pdf.Close();
                    stream.Close();
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
