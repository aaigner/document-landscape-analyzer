﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DLA
{
    class Util
    {
        public static string getTimestamp()
        {
            return DateTime.Now.ToString("yyyyMMddHHmmssffff");
        }

        public static PdfPTable listViewToPDFTable(ListView pDataSource, List<int> pColumnIndicise)
        {
            int cols = (pColumnIndicise.Count > 0) ? pColumnIndicise.Count : pDataSource.Columns.Count;
            PdfPTable pdfTable = new PdfPTable(cols);
            pdfTable.DefaultCell.Padding = 3;
            pdfTable.WidthPercentage = 100;
            pdfTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.DefaultCell.BorderWidth = 1;

            if (pColumnIndicise.Count == 0)
            {
                foreach (ColumnHeader column in pDataSource.Columns)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(column.Text));
                    pdfTable.AddCell(cell);
                }

                foreach (ListViewItem itemRow in pDataSource.Items)
                {
                    for (int i = 0; i < itemRow.SubItems.Count; i++)
                    {
                        pdfTable.AddCell(itemRow.SubItems[i].Text);
                    }
                }
            }
            else
            {
                for (int i = 0; i < pDataSource.Columns.Count; i++)
                {
                    if (pColumnIndicise.Contains(i))
                    {
                        PdfPCell cell = new PdfPCell(new Phrase(pDataSource.Columns[i].Text));
                        pdfTable.AddCell(cell);
                    }
                }

                foreach (ListViewItem itemRow in pDataSource.Items)
                {
                    for (int i = 0; i < itemRow.SubItems.Count; i++)
                    {
                        if (pColumnIndicise.Contains(i))
                        {
                            pdfTable.AddCell(itemRow.SubItems[i].Text);
                        }
                    }
                }
            }

            return pdfTable;
        }

        public static void exportPDF(string pCaption, ListView pDataSource, List<int> pColumnIndicise)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            if (fbd.ShowDialog().Equals(DialogResult.OK))
            {
                try
                {
                    string path = fbd.SelectedPath + Path.DirectorySeparatorChar + getTimestamp() + "-export.pdf";

                    using (FileStream stream = new FileStream(path, FileMode.Create))
                    {
                        iTextSharp.text.Document pdf = new iTextSharp.text.Document(PageSize.A4, 10f, 10f, 10f, 0f);

                        PdfWriter.GetInstance(pdf, stream);
                        pdf.Open();
                        pdf.Add(new Paragraph(pCaption));
                        pdf.Add(listViewToPDFTable(pDataSource, pColumnIndicise));
                        pdf.Close();
                        stream.Close();
                    }

                    MessageBox.Show("Info", "Saved report: " + path);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error", ex.Message);
                }
            }
        }
    }
}
